<div class="tuile_graph" style="width: 47.5%;">
	<h2>Charges mensuelles</h2>

	<div id="graph_revenu" ></div>

	<div id="donut-charge" style="height: 300px; width: 90%;margin-top: 40px; margin-bottom: 40px;"></div>

	<script type="text/javascript">
		/*
		* Play with this code and it'll update in the panel opposite.
		*
		* Why not try some of the options above?
		*/

		var loyer_actuel = <?php echo json_encode($loyer_actuel); ?>;
		var pension_verse = <?php echo json_encode($pension_verse); ?>;
		var total_mensualite = <?php echo json_encode($total_mensualite); ?>;
		var propal_mensualite_arr = <?php echo json_encode($propal_mensualite_arr); ?>;

		Morris.Donut({
			element: 'donut-charge',
			data: [
				{label: "Loyer actuel", value: loyer_actuel},
				{label: "Pension verse", value: pension_verse},
				{label: "Total mensualité", value: total_mensualite},
				{label: "Mensualité nouveau Crédit", value: propal_mensualite_arr},

			],
			formatter: function (x) { return x + " €"},
		});

	</script>

	<div class="tuile_graph_table">


		<div>
			<?php

				if( $type_invest != 'rp'){
					echo "<h3>Loyer actuel </h3>";
					echo '<p>' . $loyer_actuel . '€</p>';
				}
			?>
		</div>

		<div>
			<?php
				if( $pension_verse != 0){
					echo "<h3>Pension versée</h3>";
					echo '<p>' . $pension_verse . '€</p>';
				}
			?>
		</div>

		<div>
			<?php
				if( $total_mensualite != 0){
					echo "<h3>Total mensualite</h3>";
					echo $total_mensualite . '€';
				}
			?>
		</div>

		<div>
			
				<h3>Nouvelle Mensualité</h3>
				<p><?= $propal_mensualite_arr ?> €</p>
				
			
		</div>

		<div>
			<h3>Total</h3>
			<p><?= $total_charge + $propal_mensualite_arr?> €</p>
		</div>	
			
	</div>
</div>