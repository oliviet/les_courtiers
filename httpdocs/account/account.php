<?php
session_start();

require ('../inc/functions.php');
require('../calculateur.php');

logged_only();


?>

<?php require('inc/header.php'); ?>

<div class="pagecompte">

	<div  id="moncompte" class="sectioncompte">
		<div class="souscompte">

			<h2 class="titreformulaire">Votre compte <?= $_SESSION['auth']->prenom1;?> <?= $_SESSION['auth']->nom1; ?></h2>
			
			<div class="friseetape">
				<div>
					<p class="etapevalide">&#x2713;</p>
					<p class="libetape">Compléter votre dossier</p>
				</div>
								
				<div>
					<p class="etapeencours">...</p>
					<p class="libetapeencours">Choix de la proposition</p>
				</div>
				
				<div>
					<p class="etape"></p>
					<p class="libetape">Envoie du dossier</p>
				</div>
				
				<div>
					<p class="etape"></p>
					<p class="libetape">Signature du crédit</p>
				</div>
			</div>
			
			<div style="display: flex; justify-content : center;">
				<div class="imagepresentationetape">
					<img src="http://localhost/membres/image/business-2959311_1920.jpg" id="dossierimage" alt="Image Dossier"/>
				</div>
				
			<div>
				<div class="presentationetape">
					<p>Félicitation vous avez complété notre formulaire. Pour pouvoir passer à la suivante nous allons constituer ensemble votre dossier.</p>
					<br />
					<p>Ci-dessous les trois meilleurs propositions que Les Courtiers.com ont choisi pour vous. Choisissez-en une afin de poursuivre votre projet.</p>
					<br />
					<p>Un dossier complet est un argument de négociation. Nous pourrons obtenir une décote de taux.</p>
				</div>

				<h2 id="nospropositions">Nos propositions</h2>

			

				<div class="zoneproposition">
							
					
				<p>Voici trois propositions choisi par Les Courtiers.com. Cet proposition n'est pas définitive. Elle peut, après négociation, être plus avantageuse.</p>
				<form name="form" action="" method="post"> 
					<div class="divradioproposition">
						<div class="proposition">
							<input type="radio" id="propal1" name="propositionchoisie" value="propal1">
							<label for="propal1">
								<?php

								var_dump($_SESSION['$propal_1']->taux)

								echo 'n°1' . 'Taux : ' . $_SESSION['$propal_1']->taux . ' %' . 'Durée : ' . $_SESSION['$propal_1']->duree . ' ans' . 'Mensualité : ' . $_SESSION['$propal_1']->mensualite . ' €' '<br />Coût total du crédit : ' . $_SESSION['$propal_1']->cout . ' €';
								?>
							</label>
						</div>

						<div class="proposition">
							<input type="radio" id="propal2" name="propositionchoisie" value="propal2">
							<label for="propal2">n°2 Taux : 1,9% Durée : 20 ans Mensualité : 600 €<br />Coût total du crédit : 21 000 €</label>
						</div>

						<div class="proposition">
							<input type="radio" id="propal3" name="propositionchoisie" value="propal3">
							<label for="propal3">n°3 Taux : 1,95% Durée : 20 ans Mensualité : 602 €<br />Coût total du crédit : 22 000 €</label>
						</div>
					</div><!--fin div divradioproposition-->

					<button type="submit" style="width: 400px;">Choisir cette Proposition</button>
				</form>
				


				</div><!--fin div ensemble proposition-->
			</div>
			</div>
				
			
		</div><!--fin div formulaire-->
	</div><!--fin div section compte-->

<?php require('inc/accountmenu.php');?>


</div><!--fin pagecompte-->
<br />
<?php require('inc/footer.php'); ?>

<!--<form method="POST" action="">
    
   
   <div>
      <label for="mdp">Mot de passe :</label>
      <input type="password" placeholder="Votre mot de passe" name="password" />
   </div>
   
   <div>
      <label for="mdp2">Confirmation du mot de passe :</label>
      <input type="password" placeholder="Confirmez votre mdp" name="password_confirm" />
   </div>
   <br />

   <button type="submit">M'inscrire</button>

    </form>


      
      </*?php logged_only();
      
      if(!empty($_POST)){

          if(empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
              $_SESSION['flash']['danger'] = "Les mots de passes ne correspondent pas";
          }else{
              $user_id = $_SESSION['auth']->id;
              $password= password_hash($_POST['password'], PASSWORD_BCRYPT);
              require_once 'inc/db.php';
              $pdo->prepare('UPDATE users SET password = ? WHERE id = ?')->execute([$password,$user_id]);
              $_SESSION['flash']['success'] = "Votre mot de passe a bien été mis à jour";
          }

      } ?>

         <br />
         <a href="editionprofil.php">Editer mon profil</a>
         <a href="deconnexion.php">Se déconnecter</a>
         -->