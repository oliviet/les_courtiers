<?php
if(isset($_GET['id']) && isset($_GET['token'])){
	require 'inc/db';
	require 'inc/functions.php';
	$req = $pdo->prepare('SELECT * FROM users WHERE id = ? and reset_token IS NOT NULL ADN reset_token = ? and reset_at > DATE_SUB(NOW(),INTERVALLE 30 MINUTES');
    $req->execute(['id'] => $_POST['token']]);
    $user = $req->fetch();
	if($user){
		if(!empty($_POST)){
			if(!empty($_POST['password']) && $_POST['password'] == $_POST['password_confirm']){
				$password = password_hash($_POST['password'], password_BCRYPT);
				$pdo->prepare('UPDATE users SET password = ?, reset_at = NULL, reset_token = NULL')->execute([$password]);
				session_start();
				$_SESSION['flash']['success'] ='Votre mot de passe a bien été modifié';
				$_SESSION['auth'] = $user;
				header('Location: account.php');
				exit();
			}
		}
	}else{
		session_start();
		$_SESSION['flash']['error'] = "Ce token n'est pas valide";
		header('Location: login.php')
		exit();
	}
}
?>

<?php require ('header');?>

<h1>Réinitialiser mon mot de passe</h1>

<form method="POST" action="">
           
	<label for="pseudo">Nom :</label>
	<input type="text" placeholder="Nom d'Utilisateur" id="user" name="user" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>" />
				   
	<label for="mail">Mail :</label>
	<input type="email" placeholder="Votre mail" id="mail" name="mail" value="<?php if(isset($mail)) { echo $mail; } ?>" />

	<label for="mail2">Confirmation du mail :</label>
	<input type="email" placeholder="Confirmez votre mail" id="mail2" name="mail2" value="<?php if(isset($mail2)) { echo $mail2; } ?>" />

	<label for="mdp">Mot de passe :</label>
	<input type="password" placeholder="Votre mot de passe" id="password" name="password" />

	<label for="mdp2">Confirmation du mot de passe :</label>
	<input type="password" placeholder="Confirmez votre mdp" id="mdp2" name="mdp2" />

	<br />

	<input type="submit" name="forminscription" value="Je m'inscris" />

</form>


<?php require ('footer');?>