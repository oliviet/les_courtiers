<?php
require_once '../../inc/functions.php';
require_once '../../inc/db.php';
session_start();

/*
$_FILES['icone']['name']    ; //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.png).
$_FILES['icone']['type']     ;//Le type du fichier. Par exemple, cela peut être « image/png ».
$_FILES['icone']['size']    ; //La taille du fichier en octets.
$_FILES['icone']['tmp_name'] ;//L'adresse vers le fichier uploadé dans le répertoire temporaire.
$_FILES['icone']['error']    ;//Le code d'erreur, qui permet de savoir si le fichier a bien été uploadé.
*/
$maxsize = 1048576;

if ($_FILES['mon_fichier']['size'] > $maxsize) {
	$_SESSION['flash']['danger'] = 'La taille du fichier n\'est pas autorisé';
	header('Location: ../../account_doc.php');
	exit();
}

$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'pdf', );
//1. strrchr renvoie l'extension avec le point (« . »).
//2. substr(chaine,1) ignore le premier caractère de chaine.
//3. strtolower met l'extension en minuscules.
$extension_upload = strtolower(  substr(  strrchr($_FILES['mon_fichier']['name'], '.')  ,1)  );

if ( in_array($extension_upload,$extensions_valides) == TRUE ){

	echo "Extension correcte";
	}else{
	$_SESSION['flash']['danger'] = 'Le type du fichier n\'est pas autorisé';
	header('Location: ../../account_doc.php');
	exit();
	};

$id= $_SESSION['auth']->id;
$filename= 'id='. $id . 'pi_emprunteur_2' . '.'. $extension_upload ;
$date= time();
$file= './dossier/$id';

if(file_exists($file)){
	
}else{
	mkdir("dossier/$id", 0777, true);
};

$nom = "dossier/$id/$filename.{$extension_upload}";
$resultat = move_uploaded_file($_FILES['mon_fichier']['tmp_name'],$nom);

if($resultat){
	if(isset($_SESSION['up']->up_id)== $id){
		$req = $pdo->prepare('UPDATE up SET up_pi_2_fn= ? WHERE "'.$id.'" ');
		$req->execute([$filename]);

	}else{
	$req = $pdo->prepare("INSERT INTO up SET up_id= ?, up_pi_2_fn= ?");
		$req->execute([$id, $filename]);
	};
	
	$_SESSION['flash']['success'] = 'Le fichier à bien été transmis, il va être analysé';
	header('Location: ../../account_doc.php');
	exit();
}else{
	$_SESSION['flash']['danger'] = 'Une erreur est survenu, réesseyer ou contacter le service client';
	header('Location: ../../account_doc.php');
	exit();
};