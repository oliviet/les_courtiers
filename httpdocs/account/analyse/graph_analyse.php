<br />
<br />

<div class="tuile_graph" style="width: 100%;">
	<h3>Indicateur</h3>
		
		<div style="display: flex;">

			<div>

				<div id="bar_example"></div>

				<script type="text/javascript">

					var capacite_emprunt = <?php echo json_encode($capacite_emprunt); ?>;
					var rav_arr = <?php echo json_encode($rav_arr); ?>;
					
					/*
					 * Play with this code and it'll update in the panel opposite.
					 *
					 * Why not try some of the options above?
					 */
					Morris.Bar({
						element: 'bar_example',
						data: [
							{ y: 'Capacite emprunt', a: capacite_emprunt},
							{ y: 'Reste à vivre', a: rav_arr }
						],
						xkey: 'y',
						ykeys: ['a'],
						labels: [' '],
						
					});

				</script>

				<div class="tuile_graph_table">

					<div>
						<h3>Capacité d'emprunt :</h3>
						<p><?= $capacite_emprunt_arr ?> €</p>
					</div>

					<div>
						<h3>Reste à vivre :</h3>
						<p><?= $rav_arr ?> €</p>
					</div>
				
				</div>
			</div>
			<div>
				<div id="bar_endettement" style="height: 300px; width: 90%;margin-top: 40px; margin-bottom: 40px;"></div>

				<script type="text/javascript">

					var taux_endettement_arr = <?php echo json_encode($taux_endettement_arr); ?>;
					
					/*
					 * Play with this code and it'll update in the panel opposite.
					 *
					 * Why not try some of the options above?
					 */
					Morris.Bar({
						element: 'bar_endettement',
						data: [
							{ y: 'Taux endettement', a: taux_endettement_arr},
						],
						xkey: 'y',
						ykeys: ['a'],
						labels: [' '],
						formatter: function (x) { return x + " %"},
					});

				</script>
				
				<div class="tuile_graph_table">
					<div>
						<h3>Taux d'endettement :</h3>
						<p><?= $taux_endettement_arr ?> %</p>
					</div>

				</div>
			</div>
		</div>

		<div>


			<h3>Notre Analyse</h3>

			<p><?= $taux_endettement_avis ?></p>

		</div>
	</div>
</div>