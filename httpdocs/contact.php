<?php session_start(); ?>
<?php require('inc/header.php'); ?>

<br />
<br />
<br />


<div class="askfile">
	

		<?php if(array_key_exists('errors', $_SESSION)): ?>
			<div class="alert alert-danger">
				<?= implode('<br>', $_SESSION['errors']); ?>
			</div>
		<?php endif; ?>

		<?php if(array_key_exists('success', $_SESSION)): ?>
			<div class="alert alert-success">
				Votre email a bien été envoyé
			</div>
		<?php endif; ?>

		<form action="post_contact.php" method="POST">
			<div class="formulaire">
				<div style="display: flex;">
				<div class="divquestion">
				
					<label for="nom">Votre nom</label>
					<input type="text" name="nom" class="form-control" id="nom" value="<?= isset($_SESSION['inputs']['nom']) ? $_SESSION['inputs']['nom'] : '';?>" >
				</div>

				<div class="divquestion">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control" id="email" value="<?= isset($_SESSION['inputs']['email']) ? $_SESSION['inputs']['email'] : '';?>" >
				</div>
			</div>
				<div class="divquestion">
					<label for="message">Message</label>
					<textarea classe="formtext" id="message" name="message"> <?= isset($_SESSION['inputs']['message']) ? $_SESSION['inputs']['message'] : '';?></textarea>
				</div>

				<button type="submit">Envoyer</button>
			</div>
		</div>
		</form>

</div>

<br />
<br />
<br />

<?php require('inc/footer.php'); ?>

<?php 
unset($_SESSION['inputs']);
unset($_SESSION['success']);
unset($_SESSION['errors']);
?>