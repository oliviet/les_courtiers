<?php
	require ('../inc/functions.php');

	require ('phpformulaire.php');

?>

<?php require('../inc/header.php'); ?>



<style type="text/css">#A1, #A2, #A3, #A4, #A5, #A6, #A7, #A8, #A9, #A10, #A11, #A12, #A13, #A15, #A16, #A17, #A18, #A19, #A20, #A21, #A22, #A23 {display: none;}</style>

<script type="text/javascript" src="https://lescourtiers.com/javascript/script.js""></script>


<div class="askfile">
	<br/>
	<p style="font-size: 13.3px;">Grâce à ce formulaire <em class="bold_m">Les Courtiers</em> va pouvoir vous trouver la meilleur solution pour votre projet.<br/>Le formulaire est complet et toutes les informations demandées sont néccessaires pour établir une offre de crédit.<br/><br/><em class="bold_m">En vous remerciant d'avoir choisi nos services.</em></p>

	<form name="form" action="" method="post"> 

		<div id="A0" class="formulaire">

			<?php if(!empty($errors)): ?>
				<div class="alert alert_danger">
					<ul>
							<p>Vous n'avez pas rempli le formulaire correctement</p>
						<?php foreach($errors as $error): ?>
							<li><?= $error; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>	

			<h2>Votre Projet</h2>

				<div>
					<h3 class="titreformulaire" name="titreformulaire">Type de projet :</h3>
					<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>
							<em class="bold_m">Rachat de prêt :</em>
							<br />
							Réétudier les conditions de votre prêt actuel afin
							<br />
							d'en diminuer le coût.
						</span>
					</a>
				</div>

				<div class="divradio" name="divradio">
		
					<div name="q1">
					<input type="radio" id="hoix1" name="type_projet" value="terrain-seul" onclick="showRadio2()">
					<label for="hoix1">Terrain seul</label>
					</div>

					<div name="q1">
					<input type="radio" id="hoix2" name="type_projet" value="terrain-construction" onclick="showRadio2()">
					<label for="hoix2">Construction</label>
					</div>

					<div name="q1">
					<input type="radio" id="hoix3" name="type_projet" value="acquisition" onclick="showRadio2()">
					<label for="hoix3">Achat d'un bien</label>
					</div>
					
					<div name="q1">
					<input type="radio" id="hoix4" name="type_projet" value="rac" onclick="showRadio2()">
					<label for="hoix4">Rachat de prêt</label>
					</div>

				</div>	
				
				<div id="A2">
					
					<h3 class="titreformulaire">Type de bien :</h3>
					<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>	
							<em class="bold_m">Ancien :</em> Logement qui a déjà été habité.
							<br/>
							<em class="bold_m">VEFA (sur plan) :</em> Vente en l'état futur<br/>d'achèvement.
							<br/>
							<em class="bold_m">Neuf :</em> Logement jamais été habité et
							<br/>construction achevée il y a moins de 5 ans.
						</span>
					</a>

					<div class="divradio">
						
						<div>
						<input type="radio" id="ancien" name="type_bien" value="ancien" onclick="showRadio3()">
						<label for="ancien">Ancien </label>
						</div>

						<div>
						<input type="radio" id="hoix5" name="type_bien" value="ancien-travaux" onclick="showRadio3()">
						<label for="hoix5">Ancien travaux</label>
						</div>

						<div>
						<input type="radio" id="vefa" name="type_bien" value="vefa" onclick="showRadio3()">
						<label for="vefa">VEFA (sur plan)</label>
						</div>
						
						<div>
						<input type="radio" id="neuf" name="type_bien" value="neuf" onclick="showRadio3()">
						<label for="neuf">Neuf</label>
						</div>

					</div>
				</div>	

				<div id="A3">
					<h3 class="titreformulaire">Type de residence :</h3>
					<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>					
							<em class="bold_m">Résidence principale :</em> Logement où vous êtes domicilé<br/>fiscalement.
							<br/><em class="bold_m">Résidence secondaire :</em> Logement que vous n'habitez<br/>que temporairement.
							<br/><em class="bold_m">Investissement locatif :</em> Acquisition d'un bien destiné<br/>à être loué.
						</span>
					</a>
					
					<div class="divradio">
		
						<div>
						<input type="radio" id="rp" name="type_invest" value="rp" onclick="showRadio4()">
						<label class="label-sm" for="rp">Principale</label>
						</div>

						<div>
						<input type="radio" id="rs" name="type_invest" value="rs" onclick="showRadio4()">
						<label class="label-sm" for="rs">Secondaire</label>
						</div>

						<div>
						<input type="radio" id="hoix6" name="type_invest" value="rl" onclick="showRadio4()">
						<label for="hoix6">Locatif</label>
						</div>
						
						<div>
						<input type="radio" id="autre" name="type_invest" value="autre" onclick="showRadio4()">
						<label for="autre">Autre</label>
						</div>
					</div>

					<h3 class="titreformulaire">Etat d'avancement :</h3>
					<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>	
							<em class="bold_m">La promesse ou compromis de vente</em> est un engagement <br/>liant le vendeur et l'acquéreur et permettant de fixer leur<br/>accord sur le prix de vente, les conditions, la date de<br/>signature de l'acte de vente.
						</span>
					</a>
					<div class="divradio">
			
						<div>
						<input type="radio" id="signe" name="avancement" value="signe">
						<label for="signe">Promesse signé</label>
						</div>

						<div>
						<input type="radio" id="trouve" name="avancement" value="trouve">
						<label for="trouve">Bien trouvé</label>
						</div>

						<div>
						<input type="radio" id="recherche" name="avancement" value="recherche">
						<label for="recherche">En recherche</label>
						</div>
					</div>

				</div>

				<div class="divquestion">
					<label for="localisation">Localisation du bien :</label>
					<br />
					<input  id="user_input_autocomplete_address" type="text" placeholder=" Paris, 75000, ..." name="localisation" />
					<script type="text/javascript">
  // Lie le champs adresse en champs autocomplete afin que l'API puisse afficher les propositions d'adresses
  function initializeAutocomplete(id) {
    var element = document.getElementById(id);
    if (element) {
     var autocomplete = new google.maps.places.Autocomplete(element, { types: ['geocode'] });
     google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
    }
  }
 
  // Injecte les données dans les champs du formulaire lorsqu'une adresse est sélectionnée
  function onPlaceChanged() {
    var place = this.getPlace();
 
    for (var i in place.address_components) {
      var component = place.address_components[i];
      for (var j in component.types) {  
        var type_element = document.getElementById(component.types[j]);
        if (type_element) {
          type_element.value = component.long_name;
        }
      }
    }
 
    var longitude = document.getElementById("longitude");
    var latitude = document.getElementById("latitude");
    longitude.value = place.geometry.location.lng();
    latitude.value = place.geometry.location.lat();
  }
 
  // Initialisation du champs autocomplete
  google.maps.event.addDomListener(window, 'load', function() {
    initializeAutocomplete('user_input_autocomplete_address');
  });
</script>
				</div>

					<div class="divquestion">
						<label for="duree_pret">Durée du prêt souhaitée:</label>
						<br />
						<select name="duree_pret" >
							<option value="-">-</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="10">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="+25">+ 25</option>
						</select>
					</div>

				<div id="A4">

						<h2>Caractéristique du prêt racheté : </h2>
						<a href="#" class="info"><img src="../image/icone_aide.png">
							<span>	
								Capital restant dû : Montant du crédit restant à rembourser.

							</span>
						</a>
						<div class="divquestion">
							<label for="montant_pret_initial">Montant du prêt initial :</label>
							<input type="number" placeholder="Ex: 250 000 €" name="montant_pret_initial" />
							<!--<span class="validity"></span>-->
						</div>

						<div class="divquestion">
							<label for="taux_pret_initial">Taux d'intérêt du prêt initial :</label>
							<input type="number" placeholder="Ex: 1,5%" name="taux_pret_initial" />
							
						</div>

						<div class="divquestion">
							<label for="duree_initial">Durée initiale (mois) :</label>
							<input type="number" placeholder="Ex: 120 mois" name="duree_initial" />
							
						</div>

						<div class="divquestion">
							<label for="capital_restant_du">Capital restant dû :</label>
							<input type="number" placeholder="Ex: 200 000 €" name="capital_restant_du" />
							
						</div>

						<div class="divquestion">
							<label for="duree_restante">Durée restante (mois) :</label>
							<input type="number" placeholder="Ex:60 mois" name="duree_restante" />
							
						</div>
					
				</div>

				<div id="A5">

					<h2>Montants de votre Projet</h2>

					<div class="divquestion">
						<label for="montant_acquisition">Montant  de votre acquisition :</label>
						<a href="#" class="info"><img src="../image/icone_aide.png">
							<span>	
								<em class="bold_m">Montant de votre acquisition :</em> Prix estimé du bien.<br/>Si vous ne connaissez pas le montant exact,<br/>une estimation est suffisante.
							</span>
						</a>
						<br />
						<input type="number" placeholder="Ex: 250 000 €" name="montant_acquisition" />
					</div>

					<div class="divquestion">
						<label for="apport">Montant de votre apport personnel :</label>
						<br />
						<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>	
							<em class="bold_m">Apport personnel :</em> Montant que vous souhaitez apporter.<br/>Votre apport peut provenir de votre patrimoine financier,<br/>de votre épargne...
						</span>
					</a>
						<input type="number" placeholder="Ex: 20 000 €" name="apport" />
					</div>

					<div class="divquestion">
						<label for="frais_notaire">Frais de notaire :</label>
						<a href="#" class="info"><img src="../image/icone_aide.png">
							<span>	
								<em class="bold_m">Frais de notaire :</em> Egalement appelés frais de mutation,<br/> l'ensemble des sommes versées au notaire								 et sont intégrées<br/>dans le plan de financement de votre projet.

							</span>
						</a>
						<br />
						<input type="number" placeholder="Ex: 1 500 €" name="frais_notaire" />
					</div>

				</div>

				<div id="A6">

					<div class="divquestion">
						<label for="montant_construction">Montant de la construction :</label>
						<br />
						<input type="number" placeholder="Ex: 25 000 €" name="montant_construction" />
					</div>

				</div>

				<div id="A7">

					<div class="divquestion">
						<label for="montant_travaux">Montant des travaux :</label>
						<br/>
						<a href="#" class="info"><img src="../image/icone_aide.png">
							<span>	
								<em class="bold_m" >Montant des travaux </em> : Montant des devis<br/>des entreprises pour faire les travaux.
								<br />Si vous ne connaissez pas le montant,<br/>une estimation est suffisante.

							</span>
						</a>
						<input type="number" placeholder="Ex: 25 000 €" name="montant_travaux" />
					</div>

				</div>

				<div id="A8">

					<div class="divquestion">
						<label for="furtur_loyer">Montant du futur loyer mensuel :</label>
						<input type="number" placeholder="Ex: 400 €" name="furtur_loyer" />
					</div>

				</div>

			<h2>Situation Personnelle</h2>

			<div>
			<h3 class="titreformulaire">Nombre d'emprunteur(s) :</h3>
			<a href="#" class="info"><img src="../image/icone_aide.png">
				<span>	
					<em class="bold_m"> Avec un co-emprunteur :</em> Conjoint, concubin,<br />ami ou famille qui emprunte avec vous.
					<br/>Le co-emprunteur est tenu aux mêmes obligations<br/>que vous et doit participer solidairement<br/>au remboursement de l'emprunt.
				</span>
			</a>

				<div class="divradio">
					<div>
					<input type="radio" id="hoix7" name="nb_emprunteur" value="1" onclick="showRadio5()">
					<label for="hoix7">1</label>
					</div>
				
					<div>
					<input type="radio" id="hoix8" name="nb_emprunteur" value="2" onclick="showRadio5()">
					<label for="hoix8">2</label>
					</div>
				</div>
			</div>

			<div id="A9" style="width: 50%; float: left; background-color: rgba(0, 30, 70,0.1);">
				
				<div class="divradio-50">
					<div>
					<input type="radio" id="monsieur1" name="civilite_1" value="monsieur">
					<label for="monsieur1">Monsieur</label>
					</div>
				
					<div>
					<input type="radio" id="madame1" name="civilite_1" value="madame">
					<label for="madame1">Madame</label>
					</div>
				</div>
			
				<div class="divquestion">
					<label for="nom_1">Nom :</label>
					<br />
					<input type="text" placeholder="Votre Nom" name="nom_1" />
				</div>

				<div class="divquestion">
					<label for="prenom_1">Prénom :</label>
					<br />
					<input type="text" placeholder="Votre Prénom" name="prenom_1" />
				</div>

				<h3 class="titreformulaire">Situation maritale :</h3>
				<a href="#" class="info"><img src="../image/icone_aide.png">
							<span>	
								<em class="bold_m" >Contrat de mariage :</em><br/>PACS;<br/>
								Communauté d'acquêts aménagée;<br/>
								Communauté universelle;<br/>
								Séparation de biens;<br/>
								Participation aux acquêts.
							</span>
						</a>
				<div class="divradio-50">

					<div>
					<input type="radio" id="celibataire1" name="maritale_1" value="celibataire">
					<label for="celibataire1">Célibataire</label>
					</div>

					<div>
					<input type="radio" id="marie1" name="maritale_1" value="marie">
					<label for="marie1">Marié</label>
					</div>

					<div>
					<input type="radio" id="mariecontrat1" name="maritale_1" value="mariecontrat">
					<label for="mariecontrat1">Contrat</label>
					</div>
					
				</div>

				<h3 class="titreformulaire">Nationalité :</h3>
				<div class="divradio-50"">

					<div>
					<input type="radio" id="france1" name="nationalite_1" value="france">
					<label for="france1">France</label>
					</div>

					<div>
					<input type="radio" id="ue1" name="nationalite_1" value="ue">
					<label for="ue1">U.E</label>
					</div>

					<div>
					<input type="radio" id="Autre1" name="nationalite_1" value="Autre">
					<label for="Autre1">Autre</label>
					</div>			

				</div>
				
				<div class="divquestion">
				<label for="naissance_1">Date de naissance :</label>
				<br />
				<input type="date" placeholder="jj-mm-aaaa" name="naissance_1" />
				<span class="validity1"></span>
				</div>

				<div class="divquestion">
				<label for="telephone_1">Téléphone :</label>
				<br />
				<input type="tel" placeholder="Ex: 01 00 00 00 00" name="telephone_1" />
				
				</div>
			</div>

			<div id="A10" style="width: 50%; float: left; background-color: rgba(0, 30, 70,0.1);">
						
				<div class="divradio-50">
					<div>
					<input type="radio" id="monsieur2" name="civilite_2" value="monsieur">
					<label for="monsieur2">Monsieur</label>
					</div>
				
					<div>
					<input type="radio" id="madame2" name="civilite_2" value="madame">
					<label for="madame2">Madame</label>
					</div>
				</div>
			
				<div class="divquestion">
				<label for="nom_2">Nom :</label>
				<br />
				<input type="text" placeholder="Votre Nom" name="nom_2" />
				
				</div>

				<div class="divquestion">
				<label for="prenom_2">Prénom :</label>
				<br />
				<input type="text" placeholder="Votre Prénom" name="prenom_2" />
				
				</div>

				<h3 class="titreformulaire">Situation maritale :</h3>
				<div class="divradio-50">
		
					<div>
					<input type="radio" id="celibataire2" name="maritale_2" value="celibataire">
					<label for="celibataire2">Célibataire</label>
					</div>

					<div>
					<input type="radio" id="marie2" name="maritale_2" value="marie">
					<label for="marie2">Marié</label>
					</div>

					<div>
					<input type="radio" id="mariecontrat2" name="maritale_2" value="mariecontrat">
					<label for="mariecontrat2">Contrat</label>
					</div>			

				</div>

				<h3 class="titreformulaire">Nationalité :</h3>
				<div class="divradio-50">

					<div>
					<input type="radio" id="france2" name="nationalite_2" value="france">
					<label for="france2">France</label>
					</div>

					<div>
					<input type="radio" id="ue2" name="nationalite_2" value="ue">
					<label for="ue2">U.E</label>
					</div>

					<div>
					<input type="radio" id="Autre2" name="nationalite_2" value="Autre">
					<label for="Autre2">Autre</label>
					</div>
					
				</div>	

				<div class="divquestion">
				<label for="naissance_2">Date de naissance :</label>
				<br />
				<input type="date" placeholder="jj-mm-aaaa" name="naissance_2" />
				
				</div>

				<div class="divquestion">
				<label for="telephone_2">Téléphone :</label>
				<br />
				<input type="tel" placeholder="Ex: 01 00 00 00 00" name="telephone_2" />
				
				</div>
			</div>
			
				<div>
				
					<h2>Situation Professionelle</h2>

					<div id="A11" style="width: 50%; float: left;">

						<h3>Emprunteur 1</h3>

						<div class="divquestion">
							<a href="#" class="info"><img src="../image/icone_aide.png">
								<span>	
									Montant net à payer figurant sur votre bulletin de paye.
									<br />En cas de revenus variables, indiquez la moyenne 
									<br />de vos revenus sur les 3 derniers mois.
								</span>
							</a>
							
							<label for="salaire_net_mois_1">Salaire net mensuel :</label>
							<br />
							<input type="number" placeholder="Ex: 2 000 €" name="salaire_net_mois_1" />
							
						</div>

						<div class="divquestion">
							<label for="prime_net_an_1">Primes annuelles :</label>
							<a href="#" class="info"><img src="../image/icone_aide.png">
								<span>	
									Montant net à payer figurant sur votre bulletin de paye.
									<br />En cas de revenus variables, indiquez la moyenne
									<br />de vos revenus sur les 3 derniers années.
								</span>
							</a>
							<br />
							<input type="number" placeholder="Ex: 4 000 €" name="prime_net_an_1" />
							
						</div>

						<div class="divquestion">
							<label for="situation_pro_1">Situation professionnelle :</label>
							<br />
							<select name="situation_pro_1" >
								<option value="-">-</option>
								<option value="employé">Employé</option>
								<option value="ouvrier">Ouvrier</option>
								<option value="cadre">Cadre</option>
								<option value="fonctionnaire">Fonctionnaire</option>
								<option value="enseignant">Enseignant</option>
								<option value="artisan-commercant">Artisan Commercant</option>
								<option value="chef-d-entreprise">Chef d'entreprise</option>
								<option value="profession-liberale">Profession Libérale</option>
								<option value="independant">Travailleur Indépendant</option>
								<option value="intermittent">Intermittent du spectacle</option>
								<option value="agriculteur">Agriculteur</option>
								<option value="retraite">Retraité</option>
								<option value="sans-profession">Sans profession</option>
								<option value="rechercheemploi">Recherche d'emploi</option>
								<option value="etudiant">Etudiant</option>
								<option value="autre">Autre</option>
							</select>
							
						</div>

						<div class="divquestion">
							<label for="type_contrat_1">Contrat de travail :</label>
							<br />
							<select name="type_contrat_1">
								<option value="-">-</option>
								<option value="cdi">CDI</option>
								<option value="cdd">CDD</option>
								<option value="interim">Intérim</option>
								<option value="titulaire">Titulaire</option>
								<option value="autres">Autres</option>
							</select>
							
						</div>

						<div class="divquestion">
							<label for="debut_contrat_1">Date du debut de contrat :</label>
							<br />
							<input type="date" placeholder="" name="debut_contrat_1" />
							
						</div>
					
					</div>

					<div id="A12" style="width: 50%; float: left;">
					
						<h3>Emprunteur 2</h3>

						<div class="divquestion">
							<label for="salaire_net_mois_2">Salaire net mensuel :</label>
							<br />
							<input type="number" placeholder="Ex: 2 000 €" name="salaire_net_mois_2" />
							
						</div>

						<div class="divquestion">
							<label for="prime_net_an_2">Primes annuelles :</label>
							<br />
							<input type="number" placeholder="Ex: 4 000 €" name="prime_net_an_2" />
							
						</div>


						<div class="divquestion">
							<label for="situation_pro_2">Situation professionnelle :</label>
							<br />
							<select name="situation_pro_2" >
								<option value="-">-</option>
								<option value="Employé">Employé</option>
								<option value="Ouvrier">Ouvrier</option>
								<option value="Cadre">Cadre</option>
								<option value="">Fonctionnaire</option>
								<option value="">Enseignant</option>
								<option value="">Artisan Commercant</option>
								<option value="">Chef d'entreprise</option>
								<option value="">Profession Libéral</option>
								<option value="">Travailleur Indépendant</option>
								<option value="">Intermitant du spectacle</option>
								<option value="">Agriculteur</option>
								<option value="">Retraité</option>
								<option value="">Sans profession</option>
								<option value="">Recherche d'emploir</option>
								<option value="">Etudiant</option>
								<option value="">Autre</option>
							</select>
							
						</div>

						<div class="divquestion">
							<label for="type_contrat_2">Contrat de travail :</label>
							<br />
							<select name="type_contrat_2">
								<option value="-">-</option>
								<option value="cdi">CDI</option>
								<option value="cdd">CDD</option>
								<option value="interim">Intérim</option>
								<option value="autre">Titulaire</option>
								<option value="autre">Contractuelle</option>
								<option value="autre">Autre</option>
							</select>
							
						</div>

						<div class="divquestion">
							<label for="debut_contrat_2">Date du debut de contrat :</label>
							<br />
							<input type="date" placeholder="" name="debut_contrat_2" />
							
						</div>
					
					</div>

				</div><!-- fin div situation pro-->
				
				<div>
				<h2>Situation patrimoniale</h2>
				
				<div>
					<h3 class="titreformulaire">Votre logement actuel :</h3>
					<a href="#" class="info"><img src="../image/icone_aide.png">
								<span>	
									<em class="bold_m">Fonction :</em> Logement de fonction par contrat de travail
									<br /><em class="bold_m">Gratuit :</em> Hérbergement à titre gratuit.
									
								</span>
							</a>
			
					<div class="divradio">
						<div>
							<input type="radio" id="hoix9" name="logement_actuel" value="locataire" onclick="showRadio6()">
							<label for="hoix9">Locataire</label>
						</div>

						<div>
							<input type="radio" id="proprietaire" name="logement_actuel" value="proprietaire" onclick="showRadio6()">
							<label for="proprietaire">Propriétaire</label>
						</div>

						<div>
							<input type="radio" id="logement_fonct" name="logement_actuel" value="logement_fonct" onclick="showRadio6()">
							<label for="logement_fonct">Fonction</label>
						</div>
					
						<div>
							<input type="radio" id="titre_gratuit" name="logement_actuel" value="titre_gratuit" onclick="showRadio6()">
							<label for="titre_gratuit">Gratuit</label>
						</div>
					</div>
				</div>

				<div id="A13">
					<div class="divquestion">
						<label for="montant_loyer">Montant de votre loyer mensuel actuel :</label>
						<input type="text" placeholder="Ex: 300 €" name="montant_loyer" />
						
					</div>
				</div>

				<div>
				
				<!--
				<h3 class="titreformulaire">Etes vous propriétaire ?</h3>
					<div class="divradio">					
						
						<div>
							<input type="radio" id="hoix10" name="proprietaireouinon" value="ouiproprietaire" onclick="showRadio7()">
							<label for="hoix10">Oui</label>
						</div>

						<div>
							<input type="radio" id="hoix11" name="proprietaireouinon" value="nonproprietaire" onclick="showRadio7()">
							<label for="hoix11">Non</label>
						</div>
					</div>
				</div>
				-->
				<div id="A14">

					<h3 class="titreformulaire">Nombre de biens immobiliers :<h3>
						<a href="#" class="info"><img src="../image/icone_aide.png">
								<span>	
									<em class="bold_m">Valeur bien :</em> Prix d'aquisition ou estimation par un agent immobilier.
									<br /><em class="bold_m">Mensualité :</em> Si crédit en court montant de la mensualité<br/> si libre de crédit mettre 0 €.
									
								</span>
							</a>

					<div class="divradio">
						<div>
							<input type="radio" id="hoix12" name="nb_bien_immo" value="0" onclick="showRadio8()">
							<label for="hoix12">0</label>
						</div>

						<div>
							<input type="radio" id="hoix13" name="nb_bien_immo" value="1" onclick="showRadio9()">
							<label for="hoix13">1</label>
						</div>

						<div>
							<input type="radio" id="hoix14" name="nb_bien_immo" value="2" onclick="showRadio10()">
							<label for="hoix14">2</label>
						</div>

						<div>
							<input type="radio" id="hoix15" name="nb_bien_immo" value="3" onclick="showRadio11()">
							<label for="hoix15">3</label>
						</div>

						<div>
							<input type="radio" id="hoix16" name="nb_bien_immo" value="4" onclick="showRadio12()">
							<label for="hoix16">+</label>
						</div>
					</div>
				</div>

					<div id="A15">
						<h4>Bien n°1 :</h4>
							<div style="display: flex">
								<div class="divquestion_sm">
									<label for="valeur_1">Valeur bien :</label>
									<br/>
									<input type="number" placeholder="Ex: 200 000 €" name="valeur_1" />
									
								</div>

								<div class="divquestion_sm">
									<label for="mensualite_1">Mensualité crédit :</label>
									<br/>
									<input type="number" placeholder="300 €" name="mensualite_1" />
									
								</div>

								<div class="divquestion_sm" >
									<label for="duree_restante_1">Durée restante crédit :</label>
									<br/>
									<input type="number" placeholder=" Ex: 50 (mois)" name="duree_restante_1" />
									
								</div>
							</div>
					</div>

					<div id="A16">
						<h4>Bien n°2 :</h4>
						<div style="display: flex">
							<div class="divquestion_sm" style="float: left;">
								<label for="valeur_2">Valeur bien n°2 :</label>
								<br/>
								<input type="number" placeholder="Ex: 200 000 €" name="valeur_2" />
								
							</div>

							<div class="divquestion_sm" style="float: left;">
								<label for="mensualite_2">Mensualité crédit n°2 :</label>
								<br/>
								<input type="number" placeholder="300 €" name="mensualite_2" />
								
							</div>

							<div class="divquestion_sm" >
								<label for="duree_restante_2">Durée restante crédit n°2 :</label>
								<br/>
								<input type="number" placeholder=" Ex: 50 (mois)" name="duree_restante_2" />
								
							</div>
						</div>
					</div>

					<div id="A17">
						<h4>Bien n°3 :</h4>
						<div style="display: flex">
							<div class="divquestion_sm" style="float: left;">
								<label for="valeur_3">Valeur bien n°3 :</label>
								<br/>
								<input type="number" placeholder="Ex: 200 000 €" name="valeur_3" />
								
							</div>

							<div class="divquestion_sm" style="float: left;">
								<label for="mensualite_3">Mensualité n°3 :</label>
								<br/>
								<input type="number" placeholder="300 €" name="mensualite_3" />
								
							</div>

							<div class="divquestion_sm" >
								<label for="duree_restante_3">Durée restante n°3 :</label>
								<br/>
								<input type="number" placeholder=" Ex: 50 (mois)" name="duree_restante_3" />
								
							</div>
						</div>
					</div>

					<div id="A19">
						<h4>Autres biens :</h4>
						<div style="display: flex">
							<div class="divquestion_sm" style="float: left;">
								<label for="valeur_4">Valeur des biens :</label>
								<br/>
								<input type="number" placeholder="Ex: 200 000 €, 150 000€, ..." name="valeur_4" />
								
							</div>
							<div class="divquestion_sm" style="float: left;">
								<label for="mensualite_4">Mensualité des crédits :</label>
								<br/>
								<input type="number" placeholder="300 €, 500€, ..." name="mensualite_4" />
								
							</div>

							<div class="divquestion_sm" >
								<label for="duree_restante_4">Durée restante :</label>
								<br/>
								<input type="number" placeholder=" Ex: 50 (mois), 25 (mois), ..." name="duree_restante_4" />
								
							</div>
						</div>
					</div>

					<div>

						<h3 class="titreformulaire">Nombre de crédit à la consommation :<h3>

						<div class="divradio">	
							<div>
								<input type="radio" id="hoix17" name="nb_credit_conso" value="0" onclick="showRadio13()">
								<label for="hoix17">0</label>
							</div>

							<div>
								<input type="radio" id="hoix18" name="nb_credit_conso" value="1" onclick="showRadio14()">
								<label for="hoix18">1</label>
							</div>

							<div>
								<input type="radio" id="hoix19" name="nb_credit_conso" value="2" onclick="showRadio15()">
								<label for="hoix19">2</label>
							</div>

							<div>
								<input type="radio" id="hoix20" name="nb_credit_conso" value="3" onclick="showRadio16()">
								<label for="hoix20">3</label>
							</div>

						</div>
					</div>

					<div id="A20">
						<h3>Crédit consommation n°1 :</h3>
							<div style="display: flex">

								<div class="divquestion_sm">
									<label for="mensualite_cs_1">Mensualité crédit :</label>
									<br/>
									<input type="number" placeholder="300 €" name="mensualite_cs_1" />
									
								</div>

								<div class="divquestion_sm" >
									<label for="duree_cs_1">Durée restante crédit :</label>
									<br/>
									<input type="number" placeholder=" Ex: 50 (mois)" name="duree_cs_1" />
									
								</div>
							</div>
					</div>

					<div id="A21">
						<h3>Crédit consommation n°2 :</h3>
							<div style="display: flex">

								<div class="divquestion_sm">
									<label for="mensualite_cs_2">Mensualité crédit :</label>
									<br/>
									<input type="number" placeholder="300 €" name="mensualite_cs_2" />
									
								</div>

								<div class="divquestion_sm" >
									<label for="duree_cs_2">Durée restante crédit :</label>
									<br/>
									<input type="number" placeholder=" Ex: 50 (mois)" name="duree_cs_2" />
									
								</div>
							</div>
					</div>

					<div id="A22">
						<h3>Crédit consommation n°3 :</h3>
							<div style="display: flex">

								<div class="divquestion_sm">
									<label for="mensualite_cs_3">Mensualité crédit :</label>
									<br/>
									<input type="number" placeholder="300 €" name="mensualite_cs_3" />
									
								</div>

								<div class="divquestion_sm" >
									<label for="duree_cs_3">Durée restante crédit :</label>
									<br/>
									<input type="number" placeholder=" Ex: 50 (mois)" name="duree_cs_3" />
									
								</div>
							</div>
					</div>

					<div>
						<h3 class="titreformulaire">Avez-vous des pensions alimentaire versée/perçu ?</h3>
						<div class="divradio">
			
							<div>
								<input type="radio" id="hoix21" name="pension" value="ouipension" onclick="showRadio18()">
								<label for="hoix21">Oui</label>
							</div>

							<div>
								<input type="radio" id="hoix22" name="pension" value="nonpension" onclick="showRadio18()">
							<label for="hoix22">Non</label>
							</div>

						</div>
					</div>

					<div id="A23">

						<div style="display: flex">
							<div class="divquestion">
								<label for="pension_recue">Pension alimentaire reçue :</label>
								<br />
								<input type="number" placeholder="Ex: 250 €" name="pension_recue" />
							
							</div>

							<div class="divquestion">
								<label for="pension_verse">Pension alimentaire versée :</label>
								<br />
								<input type="number" placeholder="Ex: 250 €" name="pension_verse" />
								
							</div>
						</div>
					</div>

					<div class="divquestion">
						<label for="alloc_fami">Vos allocations familiales :</label>
						<br/>
						<input type="number" placeholder="400€ (mois)" name="alloc_fami" />
						
					</div>
			
			</div> <!-- fin div situation patrimoniale-->

			<h2>Précision sur le projet</h2>


			<div class="divquestion" style="text-align: center;">
			<label for="commentaire">Commentaire</label>
			<br />
			<input type="text" placeholder="" name="commentaire" style="width: 500px; height: 100px;" />
			
			</div>

			<div class="divquestion" style="text-align: center;">
			<label for="parrain">Parrain</label>
			<br />
			<input type="text" placeholder="Code: Parrain" name="parrain" />
			
			</div>

			<div>
			<h2>Information Compte</h2>	

				<div class="divquestion" style="text-align: center;">
					<label for="username">Nom d'utilisateur :</label>
					<br />
					<input type="text" placeholder="Nom d'Utilisateur" name="username"  />
				</div>
				
				<div class="divquestion" style="text-align: center;">
					<label for="email">Courriel :</label>
					<br />
					<input type="email" placeholder="abc@xyz.com" name="email"  />
				</div>
				
				<div class="divquestion" style="text-align: center;">
					<a href="#" class="info"><img src="../image/icone_aide.png">
						<span>	
							<em class="bold_m">Mot de passe</em> Les caractères autorisés sont : a-z ; A-Z; 0-9.	
						</span>
					</a>
					<label for="password">Mot de passe :</label>
					<br />
					<input type="password" placeholder="Votre mot de passe" name="password" />
				</div>

				<div class="divquestion" style="text-align: center;">
					<label for="password_confirm">Confirmation du mot de passe :</label>
					<br />
					<input type="password" placeholder="Confirmez votre mot de passe" name="password_confirm"  />
				</div>
			</div>
			</div>
			
			</div><!-- fin formulaire-->

			<div class="tableau">


			<br />
			<br />
			<br />
			<br />
			<button type="submit" style="width: 400px;">Déposer mon dossier</button>

			<p>Les informations recueillies sont traitées par Les Courtiers.com aux fins de vous transmettre une proposition commerciale. Certaines informations sont obligatoires, en cas de non réponse votre demande ne pourra pas être traitée. Vos données personnelles peuvent être transmises aux partenaires de Les Courtiers dans le cadre de l'étude de votre demande. Vous disposez d'un droit d'accès, de rectification et d'opposition, dans le respect des dispositions légales et réglementaires en vigueur, aux informations vous concernant. Pour l'exercer, un formulaire est mis à votre disposition sur notre site. Pour plus d'information concernant le traitement de vos données personnelles et de votre dossier, veuillez consulter nos mentions légales.
			</p>

		</div>
	
	
	</form>
</div><!-- fin askfile-->


<?php require('../inc/footer.php'); ?>