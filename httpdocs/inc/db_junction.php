<?php

require_once'db.php';

if(isset($_SESSION['auth'])){
	$id_client1 = $_SESSION['auth']->id_client1;
	$req_1 = $pdo->prepare('SELECT * FROM client1 WHERE id_client= ?');
	$req_1->execute([$id_client1]);
	$_SESSION['client1'] = $req_1->fetch();
};

if(isset($_SESSION['auth'])){
	$id_client2 = $_SESSION['auth']->id_client2;
	$req_2 = $pdo->prepare('SELECT * FROM client1 WHERE id_client= ?');
	$req_2->execute([$id_client2]);
	$_SESSION['client2'] = $req_2->fetch();
};

if(isset($_SESSION['auth'])){
	$id_opportunite = $_SESSION['auth']->id_opportunite;
	$req_3 = $pdo->prepare('SELECT * FROM opportunite WHERE id_opportunite= ?');
	$req_3->execute([$id_opportunite]);
	$_SESSION['opportunite'] = $req_3->fetch();
};

if(isset($_SESSION['auth'])){
	$id_rac = $_SESSION['auth']->id_rac;
	$req_4 = $pdo->prepare('SELECT * FROM rachat WHERE id_rac= ?');
	$req_4->execute([$id_rac]);
	$_SESSION['rac'] = $req_4->fetch();
};

if(isset($_SESSION['auth'])){
	$id_bien_immo_1 = $_SESSION['client1']->id_bien_immo_1;
	$req_5 = $pdo->prepare('SELECT * FROM bien_immo WHERE id_bien_immo= ?');
	$req_5->execute([$id_bien_immo_1]);
	$_SESSION['bien_immo_1'] = $req_5->fetch();
};

if(isset($_SESSION['auth'])){
	$id_bien_immo_2 = $_SESSION['client1']->id_bien_immo_2;
	$req_5 = $pdo->prepare('SELECT * FROM bien_immo WHERE id_bien_immo= ?');
	$req_5->execute([$id_bien_immo_2]);
	$_SESSION['bien_immo_2'] = $req_5->fetch();
};

if(isset($_SESSION['auth'])){
	$id_bien_immo_3 = $_SESSION['client1']->id_bien_immo_3;
	$req_6 = $pdo->prepare('SELECT * FROM bien_immo WHERE id_bien_immo= ?');
	$req_6->execute([$id_bien_immo_3]);
	$_SESSION['bien_immo_3'] = $req_6->fetch();
};

if(isset($_SESSION['auth'])){
	$id_bien_immo_4 = $_SESSION['client1']->id_bien_immo_4;
	$req_7 = $pdo->prepare('SELECT * FROM bien_immo WHERE id_bien_immo= ?');
	$req_7->execute([$id_bien_immo_4]);
	$_SESSION['bien_immo_4'] = $req_5->fetch();
};

if(isset($_SESSION['auth'])){
	$id_credit_conso_1 = $_SESSION['client1']->id_credit_conso_1;
	$req_8 = $pdo->prepare('SELECT * FROM credit_conso WHERE id_credit_conso= ?');
	$req_8->execute([$id_credit_conso_1]);
	$_SESSION['credit_conso_1'] = $req_8->fetch();
};

if(isset($_SESSION['auth'])){
	$id_credit_conso_2 = $_SESSION['client1']->id_credit_conso_2;
	$req_9 = $pdo->prepare('SELECT * FROM credit_conso WHERE id_credit_conso= ?');
	$req_9->execute([$id_credit_conso_2]);
	$_SESSION['credit_conso_2'] = $req_9->fetch();
};

if(isset($_SESSION['auth'])){
	$id_credit_conso_3 = $_SESSION['client1']->id_credit_conso_3;
	$req_10 = $pdo->prepare('SELECT * FROM credit_conso WHERE id_credit_conso= ?');
	$req_10->execute([$id_credit_conso_3]);
	$_SESSION['credit_conso_3'] = $req_10->fetch();
};

if(isset($_SESSION['auth'])){
	$id_up = $_SESSION['auth']->id;
	$req_11 = $pdo->prepare('SELECT * FROM up WHERE up_id= ?');
	$req_11->execute([$id_up]);
	$_SESSION['up'] = $req_11->fetch();
};