<?php include("../inc/header.php"); ?>

<br /><br />

<div class="">

	<h2>Prêt à taux zéro (PTZ)</h2>

	<p>Vérifié le 08 janvier 2018 - Direction de l'information légale et administrative (Premier ministre)</p>

	<p>Le prêt à taux zéro (PTZ) est un prêt aidé par l’État qui vous permet d'acheter votre logement si vous n'avez pas été propriétaire de votre résidence principale au cours des 2 dernières années, sous conditions de ressources. Le montant du PTZ dépend de la zone où vous achetez votre logement. Le prêt ne peut financer qu'une partie de l'achat, vous devez le compléter par un ou plusieurs prêts et éventuellement un apport personnel. Ce logement doit être neuf ou ancien avec des travaux.</p>

	<h3>De quoi s'agit-il ?</h3>

	<p>Le PTZ est un prêt aidé par l'État.</p>

	<p>Il peut vous permettre d'acheter votre logement si vous n'avez pas été propriétaire de votre résidence principale au cours des 2 dernières années précédant l'émission de l'offre de prêt. Le PTZ ne peut pas financer la totalité de l'opération et doit donc être complété par un ou plusieurs prêts parmi lesquels :</p>

	<ul>
	<li>un prêt d'accession sociale (PAS) ;</li>
	<li>un prêt conventionné ;</li>
	<li>un prêt immobilier bancaire ;</li>
	<li>un prêt épargne logement ;</li>
	<li>des prêts complémentaires.</li>
	</ul>

	<p>À noter :</p>

	<p>vous pouvez également compléter ces prêts avec un apport personnel</p>

	<h3>Condition</h3>

	<p>Qualité du demandeur</p>
	<p>Vous devez certifier ne pas avoir été propriétaire de votre résidence principale au cours des 2 années précédant la demande de prêt.</p>


	<p>Toutefois, cette condition n'est pas exigée si vous ou l'un des occupants du logement :</p>

	<ul>
	<li>est titulaire d'une carte mobilité inclusion portant la mention "invalidité" ou d'une carte d'invalidité ;</li>
	<li>ou perçoit une pension d'invalidité ;</li>
	<li>ou perçoit l'allocation aux adultes handicapés (AAH) ou l'allocation d'éducation de l'enfant handicapé (AEEH),</li>
	<li>ou est victime d'une catastrophe (naturelle ou technologique par exemple) qui a rendu le logement définitivement inhabitable. La demande doit être alors réalisée dans les 2 ans suivant la publication de l'arrêté constatant le sinistre.</li>
	</ul>
	  
	<p>À savoir :</p>

	<p>vous pouvez demander le transfert de votre PTZ si vous bénéficiez d'un PTZ et que vous vendez votre logement pour en acheter un nouveau.</p>

	<h3>Conditions de ressources</h3>

	<a href="">Accéder au simulateur</a>


	<p>Vos ressources ne doivent pas excéder un plafond, qui est en fonction de vos charges de famille et de la zone où vous voulez acheter.</p>

	<h3>Durée de remboursement du PTZ</h3>
	<p>La durée de remboursement du PTZ dépend de vos revenus, de la composition du ménage et de la zone géographique dans laquelle vous achetez votre futur logement.</p>

	<p>Plus vos revenus sont élevés, plus la durée du prêt est courte.</p>

	<p>Elle s'étend de 20 à 25 ans selon les cas, et comprend 2 périodes :</p>
	<ul>
	<li>la période de différé, pendant laquelle vous ne remboursez pas le PTZ (cette période est, selon vos revenus, de 5, 10 ou 15 ans),</li>
	<li>la période de remboursement du prêt, qui suit le différé, varie entre 10 et 15 ans.</li>
	</ul>

	<h3>Établissement de crédit proposant le PTZ</h3>

	<p>L'établissement doit avoir passé une convention avec l'État.</p>

	<p>Vous choisissez celui de votre choix.</p>

	<p>En revanche, l'établissement prêteur apprécie librement la solvabilité et les garanties de l'emprunteur et n'a pas l'obligation de vous accorder le PTZ.</p>

	</div>


	<!--<div class="colonne_guide">
		<h2>Liste des Guides</h2>
		<a ref="">Prêt à taux Zéro</a>			
	</div>--><!--
</div>-->


<?php include("../inc/footer.php"); ?>