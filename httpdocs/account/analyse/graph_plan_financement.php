<div class="tuile_graph" style="width: 47.5%;">

	<h2>Plan financement</h2>

	<div id="bar_finance"></div>

		<script type="text/javascript">

			var montant_total_acquisition = <?php echo json_encode($montant_total_acquisition); ?>;
			var montant_finance = <?php echo json_encode($montant_finance); ?>;
			
			/*
			 * Play with this code and it'll update in the panel opposite.
			 *
			 * Why not try some of the options above?
			 */
			Morris.Bar({
				element: 'bar_finance',
				data: [
					{ y: 'Montant total acquisition', a: montant_total_acquisition},
					{ y: 'Montant Financé', a: montant_finance },
				],
				xkey: 'y',
				ykeys: ['a'],
				labels: [' '],
				
			});

		</script>

		<div class="tuile_graph_table">

			<div>
				<h3>Montant projet total :</h3>
				<p><?= $montant_total_acquisition ?> €</p>
			</div>

			<div>
				<h3>Montant financé :</h3>
				<p><?= $montant_finance ?> €</p>
			</div>

			<div>
				<h3>Part du financement :</h3>
				<p><?= $part_finance ?> %</p>
			</div>

		</div>
	</div>
