-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  lun. 04 juin 2018 à 16:44
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lescourtiers`
--

-- --------------------------------------------------------

--
-- Structure de la table `bien_immo`
--

CREATE TABLE `bien_immo` (
  `id` int(11) NOT NULL,
  `id_bien_immo` varchar(12) NOT NULL,
  `valeur` varchar(10) NOT NULL,
  `mensualite` varchar(10) NOT NULL,
  `duree_restante` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `client1`
--

CREATE TABLE `client1` (
  `id_lign` int(11) NOT NULL,
  `id_client` varchar(10) DEFAULT NULL,
  `civilite` varchar(20) DEFAULT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `maritale` varchar(20) DEFAULT NULL,
  `nationalite` varchar(20) DEFAULT NULL,
  `naissance` varchar(10) DEFAULT NULL,
  `telephone` varchar(13) DEFAULT NULL,
  `salaire_net_mois` decimal(65,0) DEFAULT NULL,
  `prime_net_an` varchar(100) DEFAULT NULL,
  `situation_pro` varchar(20) DEFAULT NULL,
  `type_contrat` varchar(20) DEFAULT NULL,
  `debut_contrat` varchar(10) DEFAULT NULL,
  `logement_actuel` varchar(20) DEFAULT NULL,
  `montant_loyer` varchar(20) DEFAULT NULL,
  `pension` varchar(20) DEFAULT NULL,
  `pension_recue` varchar(20) DEFAULT NULL,
  `pension_verse` varchar(20) DEFAULT NULL,
  `alloc_fami` varchar(20) DEFAULT NULL,
  `nb_bien_immo` varchar(10) DEFAULT NULL,
  `id_bien_immo_1` varchar(12) DEFAULT NULL,
  `id_bien_immo_2` varchar(12) DEFAULT NULL,
  `id_bien_immo_3` varchar(12) DEFAULT NULL,
  `id_bien_immo_4` varchar(12) DEFAULT NULL,
  `id_credit_conso_1` varchar(12) DEFAULT NULL,
  `id_credit_conso_2` varchar(12) DEFAULT NULL,
  `id_credit_conso_3` varchar(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client1`
--

INSERT INTO `client1` (`id_lign`, `id_client`, `civilite`, `nom`, `prenom`, `maritale`, `nationalite`, `naissance`, `telephone`, `salaire_net_mois`, `prime_net_an`, `situation_pro`, `type_contrat`, `debut_contrat`, `logement_actuel`, `montant_loyer`, `pension`, `pension_recue`, `pension_verse`, `alloc_fami`, `nb_bien_immo`, `id_bien_immo_1`, `id_bien_immo_2`, `id_bien_immo_3`, `id_bien_immo_4`, `id_credit_conso_1`, `id_credit_conso_2`, `id_credit_conso_3`) VALUES
(124, 'aYfuw3bYqF', 'madame', 'BENCHERIF', 'Widad', 'marie', 'france', '1974-09-15', '', '1627', '', 'EmployÃ©', 'cdi', '2018-01-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 'SO8VSyVi7v', 'monsieur', 'Delaunay', 'Thibaut', 'celibataire', 'france', '2018-05-01', '0637557678', '4500', '5000', 'employÃ©', 'cdi', '2018-05-01', 'locataire', '500', 'nonpension', NULL, NULL, '', '0', 'rxZpySIw', 'M5CX20QI', 'cBUDL9Lu', 'VJ0i95oM', 'MZaZcx8k', 'Z7191Pwt', '5FX43xyF'),
(123, 'iwa2z9A1e3', 'monsieur', 'BENCHERIF', 'Mourad', 'marie', 'france', '1969-03-27', '0664347469', '1643', '', 'employÃ©', 'cdi', '2018-01-12', 'locataire', '331', 'nonpension', '', '', '0', '0', 'WFbkCFR7', '5xg1S3Xl', 'Fdv2jQyb', 'cErSCAaD', 'EkaDCLgg', 'GGDRfVkJ', 'elu7yE9v');

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmation_token` varchar(60) DEFAULT NULL,
  `confirmed_at` datetime(6) DEFAULT NULL,
  `id_client1` varchar(10) DEFAULT NULL,
  `id_client2` varchar(10) DEFAULT NULL,
  `id_opportunite` varchar(10) DEFAULT NULL,
  `id_rac` varchar(10) DEFAULT NULL,
  `id_conseiller` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id`, `username`, `email`, `password`, `confirmation_token`, `confirmed_at`, `id_client1`, `id_client2`, `id_opportunite`, `id_rac`, `id_conseiller`) VALUES
(51, 'test1', 'thibaut.delaunay@lescourtiers.com', '$2y$10$IpIvP/3EqgIbEnm5g5V68eQL9DAp5qGItURhBz84AKb3BId0oUo1C', NULL, '2018-05-28 19:17:51.000000', 'SO8VSyVi7v', 'AAbASU5H6S', '8urIebRY', 'XoUczaX4', '1'),
(52, 'client1', 'client1@hotmail.fr', '$2y$10$zTygV3xORBHMXXZf4bQvoubm7QR49noxremFrXZmmZA19Z8c/c9F.', NULL, '2018-06-01 00:00:00.000000', 'iwa2z9A1e3', 'aYfuw3bYqF', 'BDhoR4hp', 'q40rrWcK', '1');

-- --------------------------------------------------------

--
-- Structure de la table `conseiller`
--

CREATE TABLE `conseiller` (
  `id` int(10) NOT NULL,
  `id_conseiller` varchar(10) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `telephone` int(10) NOT NULL,
  `mail` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `conseiller`
--

INSERT INTO `conseiller` (`id`, `id_conseiller`, `nom`, `prenom`, `telephone`, `mail`) VALUES
(1, '1', 'Delaunay', 'Thibaut', 681378226, 'thibaut.delaunay@lescourtiers.com');

-- --------------------------------------------------------

--
-- Structure de la table `credit_conso`
--

CREATE TABLE `credit_conso` (
  `id` int(11) NOT NULL,
  `id_credit_conso` varchar(12) NOT NULL,
  `mensualite` varchar(10) NOT NULL,
  `duree_restante` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `opportunite`
--

CREATE TABLE `opportunite` (
  `id` int(10) NOT NULL,
  `id_opportunite` varchar(10) NOT NULL,
  `duree_pret` varchar(20) DEFAULT NULL,
  `type_projet` varchar(20) DEFAULT NULL,
  `type_bien` varchar(20) DEFAULT NULL,
  `type_invest` varchar(20) DEFAULT NULL,
  `avancement` varchar(20) DEFAULT NULL,
  `localisation` varchar(100) DEFAULT NULL,
  `montant_acquisition` varchar(50) DEFAULT NULL,
  `apport` varchar(50) DEFAULT NULL,
  `frais_notaire` varchar(50) DEFAULT NULL,
  `montant_construction` varchar(50) DEFAULT NULL,
  `montant_travaux` varchar(50) DEFAULT NULL,
  `futur_loyer` varchar(50) DEFAULT NULL,
  `nb_emprunteur` varchar(2) DEFAULT NULL,
  `commentaire` varchar(1000) DEFAULT NULL,
  `code_parrain` varchar(10) DEFAULT NULL,
  `id_propal_1` varchar(20) DEFAULT NULL,
  `id_propal_2` varchar(20) DEFAULT NULL,
  `id_propal_3` varchar(20) DEFAULT NULL,
  `id_propal_choice` varchar(20) DEFAULT NULL,
  `step_1` varchar(11) DEFAULT NULL,
  `step_2` varchar(11) DEFAULT NULL,
  `step_3` varchar(11) DEFAULT NULL,
  `step_4` varchar(11) DEFAULT NULL,
  `step_5` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `opportunite`
--

INSERT INTO `opportunite` (`id`, `id_opportunite`, `duree_pret`, `type_projet`, `type_bien`, `type_invest`, `avancement`, `localisation`, `montant_acquisition`, `apport`, `frais_notaire`, `montant_construction`, `montant_travaux`, `futur_loyer`, `nb_emprunteur`, `commentaire`, `code_parrain`, `id_propal_1`, `id_propal_2`, `id_propal_3`, `id_propal_choice`, `step_1`, `step_2`, `step_3`, `step_4`, `step_5`) VALUES
(69, '8urIebRY', '20', 'terrain-seul', '0', 'rp', 'signe', '8 Rue Lancret, Paris, France', '200000', '20000', '15000', '', '', '0', '1', 'coucou', '', 'Ki6oZfzk', NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL),
(70, 'BDhoR4hp', '20', 'acquisition', 'ancien', 'rp', 'signe', 'Colombes92', '250000', '50000', '20000', '', '', '0', '2', '', 'SMADI', 'JSJrYaJn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `propal`
--

CREATE TABLE `propal` (
  `id` int(11) NOT NULL,
  `id_propal` varchar(11) NOT NULL,
  `banque` varchar(20) NOT NULL,
  `taux` varchar(11) NOT NULL,
  `duree` int(11) NOT NULL,
  `mensualite` varchar(20) DEFAULT NULL,
  `cout` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `propal`
--

INSERT INTO `propal` (`id`, `id_propal`, `banque`, `taux`, `duree`, `mensualite`, `cout`) VALUES
(166, 'FkDd3hfP', 'BNP Paribas', '1.7', 20, '73.769763267328', '2 705'),
(167, 'IB7Mpg65', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162'),
(168, 'Ncz3FdI3', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(169, 'hVYo6eyg', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(170, 'T7KHWvpv', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(171, 'rK67GAYt', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(172, 'GtYfPF8C', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(173, 'JSJrYaJn', 'BNP Paribas', '1.6', 20, '1071.7480298604', '37 220'),
(174, 'I9py57n2', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162'),
(175, 'HCz2hiEi', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162'),
(176, 'lcoTN5lI', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162'),
(177, 'B5Uq0ezV', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162'),
(178, 'Ki6oZfzk', 'BNP Paribas', '1.7', 20, '959.00692247526', '35 162');

-- --------------------------------------------------------

--
-- Structure de la table `rachat`
--

CREATE TABLE `rachat` (
  `id` int(10) NOT NULL,
  `id_rac` varchar(10) NOT NULL,
  `montant_pret_initial` varchar(10) DEFAULT NULL,
  `taux_pret_initial` varchar(10) DEFAULT NULL,
  `duree_initial` varchar(10) DEFAULT NULL,
  `capital_restant_du` varchar(10) DEFAULT NULL,
  `duree_restante` varchar(10) DEFAULT NULL,
  `nb_emprunteur` varchar(10) DEFAULT NULL,
  `propal_1` varchar(20) DEFAULT NULL,
  `propal_2` varchar(20) DEFAULT NULL,
  `propal_3` varchar(20) DEFAULT NULL,
  `step_1` varchar(11) DEFAULT NULL,
  `step_2` varchar(11) DEFAULT NULL,
  `step_3` varchar(11) DEFAULT NULL,
  `step_4` varchar(11) DEFAULT NULL,
  `step_5` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `taux`
--

CREATE TABLE `taux` (
  `id` int(10) NOT NULL,
  `banque` text NOT NULL,
  `duree` varchar(20) NOT NULL,
  `revenu` varchar(20) NOT NULL,
  `taux` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `taux`
--

INSERT INTO `taux` (`id`, `banque`, `duree`, `revenu`, `taux`) VALUES
(1, 'BNP Paribas', '20', 'r1', '1.5'),
(2, 'CFF', '20', 'r1', '2.1'),
(3, 'BNP Paribas', '20', 'r2', '1.6'),
(4, 'BNP Paribas', '20', 'r3', '1.6'),
(5, 'BNP Paribas', '20', 'r4', '1.7'),
(6, 'BNP Paribas', '20', 'r5', '1.6'),
(7, 'BNP Parabis', '20', 'r6', '1.2'),
(8, 'BNP Parabis', '20', 'r6', '1.1'),
(9, 'BNP Paribas', '15', 'r1', '1.1'),
(10, 'BNP Paribas', '15', 'r2', '0.9'),
(11, 'CFF', '15', 'r1', '1.19'),
(13, 'Société Générale', '15', 'r1', '1.3');

-- --------------------------------------------------------

--
-- Structure de la table `up`
--

CREATE TABLE `up` (
  `id` int(11) NOT NULL,
  `up_id` int(10) DEFAULT NULL,
  `up_compromis_fn` varchar(50) DEFAULT NULL,
  `up_compromis_ev` varchar(1) DEFAULT NULL,
  `up_pi_1_fn` varchar(50) DEFAULT NULL,
  `up_pi_1_ev` varchar(1) DEFAULT NULL,
  `up_pi_2_fn` varchar(50) DEFAULT NULL,
  `up_pi_2_ev` varchar(1) DEFAULT NULL,
  `up_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bien_immo`
--
ALTER TABLE `bien_immo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client1`
--
ALTER TABLE `client1`
  ADD PRIMARY KEY (`id_lign`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `conseiller`
--
ALTER TABLE `conseiller`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `credit_conso`
--
ALTER TABLE `credit_conso`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `opportunite`
--
ALTER TABLE `opportunite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `propal`
--
ALTER TABLE `propal`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rachat`
--
ALTER TABLE `rachat`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `taux`
--
ALTER TABLE `taux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `up`
--
ALTER TABLE `up`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `bien_immo`
--
ALTER TABLE `bien_immo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT pour la table `client1`
--
ALTER TABLE `client1`
  MODIFY `id_lign` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT pour la table `conseiller`
--
ALTER TABLE `conseiller`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `credit_conso`
--
ALTER TABLE `credit_conso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `opportunite`
--
ALTER TABLE `opportunite`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT pour la table `propal`
--
ALTER TABLE `propal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT pour la table `rachat`
--
ALTER TABLE `rachat`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `taux`
--
ALTER TABLE `taux`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `up`
--
ALTER TABLE `up`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
