<?php
require_once ('../inc/functions.php');
require_once ('../inc/db.php');

if(session_status() == PHP_SESSION_NONE){
	session_start();
}

/*emprunteur 1*/
$salaire_1 = $_SESSION['client1']->salaire_net_mois;
$salaire_1_arr = number_format($salaire_1, 0, ',', ' ');
$prime_1 = $_SESSION['client1']->prime_net_an/12;
$prime_1_arr = number_format($prime_1, 0, ',', ' ');
$revenu_1 = $prime_1+$salaire_1;

/*emprunteur2*/
if (isset($_SESSION['client2']->civilite)) {
		$salaire_2 = $_SESSION['client2']->salaire_net_mois;
		$prime_2 = $_SESSION['client2']->prime_net_an/12;

		$revenu_2 = $prime_2+$salaire_2;
	};

/* revenu locatif*/
$loyer_futur = ($_SESSION['opportunite']->futur_loyer)*0.7;

/*total revenu*/
if (isset($_SESSION['client2']->civilite)) {
		$total_revenu = $revenu_1 + $revenu_2 + $loyer_futur;

}else{
		$total_revenu = $revenu_1 + $loyer_futur;
};

$total_revenu_arr=  number_format($total_revenu, 0, ',', ' ');

/*Charges emprunteur 1*/
$type_invest = $_SESSION['opportunite']->type_invest;

$pension_verse = $_SESSION['client1']->pension_verse;

if(isset($pension_verse)){
	
}else{
	$pension_verse = 0;
}






$loyer_actuel = $_SESSION['client1']->montant_loyer;

if(isset($_SESSION['bien_immo_1']->mensualite)){
	$mensualite_1 = $_SESSION['bien_immo_1']->mensualite;
}else{
	$mensualite_1 = 0;
};
if(isset($_SESSION['bien_immo_2']->mensualite)){
	$mensualite_2 = $_SESSION['bien_immo_2']->mensualite;
}else{
	$mensualite_2 = 0;
};
if(isset($_SESSION['bien_immo_3']->mensualite)){
	$mensualite_3 = $_SESSION['bien_immo_3']->mensualite;
}else{
	$mensualite_3 = 0;
};
if(isset($_SESSION['bien_immo_4']->mensualite)){
	$mensualite_4 = $_SESSION['bien_immo_4']->mensualite;
}else{
	$mensualite_4 = 0;
};

$total_mensualite = $mensualite_1 + $mensualite_2 + $mensualite_3 + $mensualite_4;

if($type_invest == 'rp'){
	$total_charge = $pension_verse + $total_mensualite;
}else{
	$total_charge = $loyer_actuel + $total_mensualite;
};

$capacite_emprunt = ($total_revenu - $total_charge) *0.33;
$capacite_emprunt_arr = number_format($capacite_emprunt, 0, ',', ' ');


/*Projet*/


/*Montant projet total*/
$montant_acquisition = $_SESSION['opportunite']->montant_acquisition;
$frais_notaire = $_SESSION['opportunite']->frais_notaire;
$type_projet = $_SESSION['opportunite']->type_projet;

if($type_projet == 'terrain-seul' || 'acquisition'){
	$montant_total_acquisition = $montant_acquisition + $frais_notaire;
};
/*part de financement*/
$apport = $_SESSION['opportunite']->apport;
$montant_finance = $montant_total_acquisition - $apport;
$part_finance = ($montant_finance/$montant_total_acquisition)*100;
$part_finance = number_format($part_finance, 0, ',', ' ');

$revenu_an = $total_revenu * 12;

if($revenu_an >= 0 && $revenu_an < 20000){
	$revenu_cat = 'r0';
};
if($revenu_an >= 20000 && $revenu_an < 29999){
	$revenu_cat = 'r1';
};
if($revenu_an >= 30000 && $revenu_an < 39999){
	$revenu_cat = 'r2';
};
if($revenu_an >= 40000 && $revenu_an < 49999){
	$revenu_cat = 'r3';
};
if($revenu_an >= 50000 && $revenu_an < 59999){
	$revenu_cat = 'r4';
};
if($revenu_an >= 60000 && $revenu_an < 69999){
	$revenu_cat = 'r5';
};
if($revenu_an >= 70000 ){
	$revenu_cat = 'r6';
};

$duree_pret = $_SESSION['opportunite']->duree_pret;

// Selection proposition

/*if(isset($_SESSION['opportunite']->id_propal_1)){
	$id_propal_1 = $_SESSION['opportunite']->id_propal_1;
	
	$req = $pdo->prepare('SELECT * FROM propal WHERE id_propal = ?');
	$req->execute([$id_propal_1]);
	
	$_SESSION['$propal_1'] = $req->fetch();

}else{*/
	//Proposition 1
	$id_propal_1 = str_random2 (8);
	$id_opportunite = $_SESSION['opportunite']->id_opportunite;

	var_dump($id_propal_1);
	var_dump($id_opportunite);

	$req = $pdo->prepare("UPDATE opportunite SET id_propal_1 = ? WHERE id_opportunite = ?");
	$req->execute([$id_propal_1, $id_opportunite]);

	var_dump($revenu_cat);
	var_dump($duree_pret);

	$req = $pdo->prepare('SELECT * FROM taux WHERE revenu = ? AND duree = ? ORDER BY taux LIMIT 0,1;');
	$req->execute([$revenu_cat, $duree_pret]);
	$_SESSION['taux_1'] = $req->fetch();

	$taux_1 = $_SESSION['taux_1']->taux;
	$banque_1= $_SESSION['taux_1']->banque;


	$mensualite_1_1= (($montant_finance * $taux_1 / 1200) /(1- pow(1+$taux_1/1200, -$duree_pret*12))) /*+ ($_POST['ta']/1200*$montant_finance)*/;
	//$mensualite_1 = number_format($mensualite_1_1, 0, ',', ' ');

	$cout_1_1 = $mensualite_1_1 * $duree_pret * 12 - $montant_finance;
	$cout_1 = number_format($cout_1_1, 0, ',', ' ');

	$req = $pdo->prepare("INSERT INTO propal SET id_propal = ?, banque = ?, taux = ?, duree = ?, mensualite = ?, cout = ? ");
	$req->execute([$id_propal_1, $banque_1, $taux_1, $duree_pret, $mensualite_1_1, $cout_1]);
							
	$req = $pdo->prepare('SELECT * FROM propal WHERE id_propal = ?');
	$req->execute([$id_propal_1]);
	$_SESSION['$propal_1'] = $req->fetch();
//};

//Analyse taux d'endettement

//Calcule Taux d'endettement avec nouvelle mensualité

$propal_mensualite = $_SESSION['$propal_1']->mensualite;
$propal_mensualite_arr =  number_format($propal_mensualite, 2, ',', ' ');

$taux_endettement = ($total_charge + $propal_mensualite) / $total_revenu *100;
$taux_endettement_arr = number_format($taux_endettement, 2, ',', ' ');


if($taux_endettement > 36){
	$taux_endettement_avis = 'Impossible nous allons vous proposer une autre solution';
};
if(($taux_endettement <= 36) AND ($taux_endettement >= 30)){
	$taux_endettement_avis = 'Taux élevé mais possible';
};
if ($taux_endettement < 30) {
			$taux_endettement_avis = 'Le taux d\'endettement est faible' ;
};

$taux_endettement_inverse = 100 -$taux_endettement_arr;

//Analyse RAV

$rav = $total_revenu-$total_charge - $propal_mensualite;
$rav_arr = number_format($rav, 2, ',', '');


	//Proposition 2*/
	/*$id_propal_2 = str_random2 (8);
	
	$req = $pdo->prepare("UPDATE opportunite SET id_propal_2 = ? WHERE id_opportunite = ?");
	$req->execute([$id_propal_2, $id_opportunite]);

	$req = $pdo->prepare('SELECT * FROM taux WHERE revenu = ? AND duree = ? ORDER BY taux LIMIT 1,1;');
	$req->execute([$revenu_cat, $duree_pret]);
	$_SESSION['taux_2'] = $req->fetch();

	$taux_2 = $_SESSION['taux_2']->taux;
	$banque_2= $_SESSION['taux_2']->banque;

	$mensualite_2_1= (($montant_finance * $taux_2 / 1200) /(1- pow(1+$taux_2/1200, -$duree_pret*12))) /*+ ($_POST['ta']/1200*$montant_finance);
	$mensualite_2= number_format($mensualite_2_1, 0, ',', ' ');

	$cout_2_1 = $mensualite_2_1 * $duree_pret * 12 - $montant_finance;
	$cout_2 = number_format($cout_2_1, 0, ',', ' ');

	$req = $pdo->prepare("INSERT INTO propal SET id_propal = ?, banque = ?, taux = ?, duree = ?, mensualite = ?, cout = ? ");
	$req->execute([$id_propal_2, $banque_2, $taux_2, $duree_pret, $mensualite_2, $cout_2]);
							
	$req = $pdo->prepare('SELECT * FROM propal WHERE id_propal = ?');
	$req->execute([$id_propal_2]);
	$_SESSION['$propal_2'] = $req->fetch();

	//Proposition 3
	$id_propal_3 = str_random2 (8);
	$id_opportunite = $_SESSION['opportunite']->id_opportunite;
	
	$req = $pdo->prepare("UPDATE opportunite SET id_propal_3 = ? WHERE id_opportunite = ?");
	$req->execute([$id_propal_3, $id_opportunite]);

	$req = $pdo->prepare('SELECT * FROM taux WHERE revenu = ? AND duree = ? ORDER BY taux LIMIT 2,1;');
	$req->execute([$revenu_cat, $duree_pret]);
	$_SESSION['taux_3'] = $req->fetch();

	$taux_3 = $_SESSION['taux_3']->taux;
	$banque_3= $_SESSION['taux_3']->banque;

	$mensualite_3_1= (($montant_finance * $taux_3 / 1200) /(1- pow(1+$taux_3/1200, -$duree_pret*12))) /*+ ($_POST['ta']/1200*$montant_finance);
	$mensualite_3= number_format($mensualite_3_1, 0, ',', ' ');

	$cout_3_1 = $mensualite_3_1 * $duree_pret * 12 - $montant_finance;
	$cout_3 = number_format($cout_3_1, 0, ',', ' ');

	$req = $pdo->prepare("INSERT INTO propal SET id_propal = ?, banque = ?, taux = ?, duree = ?, mensualite = ?, cout = ? ");
	$req->execute([$id_propal_3, $banque_3, $taux_3, $duree_pret, $mensualite_3, $cout_3]);
							
	$req = $pdo->prepare('SELECT * FROM propal WHERE id_propal = ?');
	$req->execute([$id_propal_3]);
	$_SESSION['$propal_3'] = $req->fetch();
*/