<?php
	
require_once ('../inc/functions.php');
	
session_start();

	if(!empty($_POST)){
	
		$errors = array();
		require_once '../inc/db.php';
	
		if(empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])){
			$errors['username'] = "votre pseudo est invalide (alphanumérique)";
		} else {
			$req = $pdo->prepare('SELECT id FROM compte WHERE username = ?');
			$req->execute([$_POST['username']]);
			$user=$req->fetch();
			if($user){
				$errors['username'] = 'Ce pseudo est déja pris';
			}
		}
	
	if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
		$errors['email']="Votre email n'est pas valide";
		} else {
			$req = $pdo->prepare('SELECT id FROM compte WHERE email = ?');
			$req->execute([$_POST['email']]);
			$user=$req->fetch();
			if($user){
				$errors['email']='Cette email est déja pris';
			}
		}
	
	if(empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
		$errors['password'] = "Vous devez rentrer un mot de passe valide";
	}

	/*if(empty($_POST['localisation']) || !preg_match('/^[a-zA-Z0-9]+$/', $_POST['localisation'])){
		$errors['localisation'] = "Localisation invalide (alphanumérique)";
	}

*/	
	if( ! isset( $_POST['pension_recue'] ) ) $_POST['pension_recue'] = 0;

	if( ! isset( $_POST['type_bien'] ) ) $_POST['type_bien'] = 0;

	if( ! isset( $_POST['futur_loyer'] ) ) $_POST['futur_loyer'] = 0;

	if( ! isset( $_POST['civilite_2'] ) ) $_POST['civilite_2'] = 0;

	if( ! isset( $_POST['maritale_2'] ) ) $_POST['maritale_2'] = 0;

	if( ! isset( $_POST['nationalite_2'] ) ) $_POST['nationalite_2'] = 0;

	if(empty($errors)){
		
		$req = $pdo->prepare("INSERT INTO compte SET 
												username= ?,
												password= ?,
												email= ?,
												confirmation_token= ?,
												id_client1= ?,
												id_client2= ?,
												id_opportunite= ?,
												id_rac= ?,
												id_conseiller=?
												"
		);

		$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
		$token = str_random(60);
		$id_client1 = str_random1(10);
		$id_client2 = str_random1(10);
		$id_opportunite = str_random2 (8);
		$id_rac = str_random2 (8);
		$id_conseiller = 00001;
		$id_bien_immo_1= str_random2 (8);
		$id_bien_immo_2= str_random2 (8);
		$id_bien_immo_3= str_random2 (8);
		$id_bien_immo_4= str_random2 (8);
		$id_credit_conso_1= str_random2 (8);
		$id_credit_conso_2= str_random2 (8);
		$id_credit_conso_3= str_random2 (8);



		$req->execute([
					$_POST['username'],
					$password,
					$_POST['email'],
					$token,
					$id_client1,
					$id_client2,
					$id_opportunite,
					$id_rac,
					$id_conseiller
		]);
		/*info client n°1 dans client1 + patrimoine*/
		$req = $pdo->prepare("INSERT INTO client1 SET 
												id_client= ?,
												civilite= ?,
												nom= ?,
												prenom= ?,
												maritale= ?,
												nationalite= ?,
												naissance= ?,
												telephone= ?,
												salaire_net_mois= ?,
												prime_net_an= ?,
												situation_pro= ?,
												type_contrat= ?,
												debut_contrat= ?,
												logement_actuel= ?,
												montant_loyer= ?,
												pension= ?,
												pension_recue= ?,
												pension_verse= ?,
												alloc_fami= ?,
												nb_bien_immo= ?,
												id_bien_immo_1= ?,
												id_bien_immo_2= ?,
												id_bien_immo_3= ?,
												id_bien_immo_4= ?,
												id_credit_conso_1= ?,
												id_credit_conso_2= ?,
												id_credit_conso_3= ?
												"
		);


		$req->execute([
					$id_client1,
					$_POST['civilite_1'],
					$_POST['nom_1'],
					$_POST['prenom_1'],
					$_POST['maritale_1'],
					$_POST['nationalite_1'],
					$_POST['naissance_1'],
					$_POST['telephone_1'],
					$_POST['salaire_net_mois_1'],
					$_POST['prime_net_an_1'],
					$_POST['situation_pro_1'],
					$_POST['type_contrat_1'],
					$_POST['debut_contrat_1'],
					$_POST['logement_actuel'],
					$_POST['montant_loyer'],
					$_POST['pension'],
					$_POST['pension_recue'],
					$_POST['pension_verse'],
					$_POST['alloc_fami'],
					$_POST['nb_bien_immo'],
					$id_bien_immo_1,
					$id_bien_immo_2,
					$id_bien_immo_3,
					$id_bien_immo_4,
					$id_credit_conso_1,
					$id_credit_conso_2,
					$id_credit_conso_3					
		]);
		/*info client n°2 dans client1*/

		if($_POST['nb_emprunteur'] == 2){

			$req = $pdo->prepare("INSERT INTO client1 SET 
												id_client= ?,
												civilite= ?,
												nom= ?,
												prenom= ?,
												maritale= ?,
												nationalite= ?,
												naissance= ?,
												telephone= ?,
												salaire_net_mois= ?,
												prime_net_an= ?,
												situation_pro= ?,
												type_contrat= ?,
												debut_contrat= ?
												"
		);
			$req->execute([
					$id_client2,
					$_POST['civilite_2'],
					$_POST['nom_2'],
					$_POST['prenom_2'],
					$_POST['maritale_2'],
					$_POST['nationalite_2'],
					$_POST['naissance_2'],
					$_POST['telephone_2'],
					$_POST['salaire_net_mois_2'],
					$_POST['prime_net_an_2'],
					$_POST['situation_pro_2'],
					$_POST['type_contrat_2'],
					$_POST['debut_contrat_2']
					
			]);
		};
		

		/*info opportunite dans opportunite */
		if($_POST['type_projet'] != 'rac'){
			$req = $pdo->prepare("INSERT INTO opportunite SET 
												id_opportunite= ?,
												duree_pret= ?,
												type_projet= ?,
												type_bien= ?,
												type_invest= ?,
												avancement= ?,										
												localisation= ?,
												montant_acquisition= ?,
												apport= ?,
												frais_notaire= ?,
												montant_construction= ?,
												montant_travaux= ?,
												futur_loyer= ?,
												nb_emprunteur= ?,
												commentaire= ?,
												code_parrain= ?				
												"
		);

			$req->execute([
					$id_opportunite,
					$_POST['type_projet'],
					$_POST['duree_pret'],
					$_POST['type_bien'],
					$_POST['type_invest'],
					$_POST['avancement'],
					$_POST['localisation'],
					$_POST['montant_acquisition'],
					$_POST['apport'],
					$_POST['frais_notaire'],
					$_POST['montant_construction'],
					$_POST['montant_travaux'],
					$_POST['futur_loyer'],
					$_POST['nb_emprunteur'],
					$_POST['commentaire'],
					$_POST['parrain']				
			]);
		};

		/*info rac dans rachat */
		if($_POST['type_projet'] == 'rac'){ 
				$req = $pdo->prepare("INSERT INTO rachat SET 
												id_rac= ?,
												montant_pret_initial= ?,
												taux_pret_initial= ?,
												duree_initial= ?,
												capital_restant_du= ?,
												duree_restante= ?,
												nb_emprunteur= ?
												"
			);
				$req->execute([
					$id_rac,
					$_POST['montant_pret_initial'],
					$_POST['taux_pret_initial'],
					$_POST['duree_initial'],
					$_POST['capital_restant_du'],
					$_POST['duree_restante'],
					$_POST['nb_emprunteur']
			]);
		};

		/*info credit 1 dans bien immo */
		if($_POST['nb_bien_immo'] == (1 || 2 || 3 || 4 )){ 
			$req = $pdo->prepare("INSERT INTO bien_immo SET 
												id_bien_immo= ?,
												valeur= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_bien_immo_1,
				$_POST['valeur_1'],
				$_POST['mensualite_1'],
				$_POST['duree_restante_1']	
			]);
		};

		/*info credit 2 dans bien immo */
		if($_POST['nb_bien_immo'] == (2 || 3 || 4)){
			$req = $pdo->prepare("INSERT INTO bien_immo SET 
												id_bien_immo= ?,
												valeur= ?,
												mensualite= ?,
												duree_restante= ?
												"
		);
			$req->execute([
				$id_bien_immo_2,
				$_POST['valeur_2'],
				$_POST['mensualite_2'],
				$_POST['duree_restante_2']			
			]);
		};

		/*info credit 3 dans bien immo */
		if($_POST['nb_bien_immo'] == (3 || 4 )){
			$req = $pdo->prepare("INSERT INTO bien_immo SET 
												id_bien_immo= ?,
												valeur= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_bien_immo_3,
				$_POST['valeur_3'],
				$_POST['mensualite_3'],
				$_POST['duree_restante_3']					
			]);
		};

		/*info credit 4 dans bien immo */
		if($_POST['nb_bien_immo'] == 4 ){
			$req = $pdo->prepare("INSERT INTO bien_immo SET 
												id_bien_immo= ?,
												valeur= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_bien_immo_4,
				$_POST['valeur_4'],
				$_POST['mensualite_4'],
				$_POST['duree_restante_4']					
			]);
		};

		/*info credit 1 dans credit conso */
		if($_POST['nb_credit_conso'] == (1 || 2 || 3 )){ 
			$req = $pdo->prepare("INSERT INTO credit_conso SET 
												id_credit_conso= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_credit_conso_1,
				$_POST['mensualite_cs_1'],
				$_POST['duree_cs_1']
			]);
		};

		/*info credit 2 dans credit conso */
		if($_POST['nb_credit_conso'] == (2 || 3 )){
			$req = $pdo->prepare("INSERT INTO credit_conso SET 
												id_credit_conso= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_credit_conso_2,
				$_POST['mensualite_cs_2'],
				$_POST['duree_cs_2']			
			]);
		};

		/*info credit 3 dans credit conso */
		if($_POST['nb_credit_conso'] == 3 ){
			$req = $pdo->prepare("INSERT INTO credit_conso SET 
												id_credit_conso= ?,
												mensualite= ?,
												duree_restante= ?												
												"
		);
			$req->execute([
				$id_credit_conso_3,
				$_POST['mensualite_cs_3'],
				$_POST['duree_cs_3']				
			]);
		};

		$user_id = $id_client1;
		mail($_POST['email'],'Confirmation de votre compte : Les Courtiers.com',"Merci d'avoir choisie nos services Les Courtiers.com.\n\nAfin de valider et de pouvoir accéder à votre compte merci de cliquer sur ce lien :
			\n\nhttps://lescourtiers.com/connexion/confirm.php?id=$user_id&token=$token");
			$_SESSION['flash']['success']='un email de confirmation vous a été envoyé pour valider votre compte';
			header('Location: ../connexion/login.php');
			exit();
	}

};
?>