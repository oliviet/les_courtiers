<?php
session_start();

require ('../inc/functions.php');
/*
require ('../inc/functionsaccount.php');
*/

logged_only();
?>

<?php require('../inc/header.php'); ?>

<div style="display: flex;">
	<?php require('menu_account.php');?>

	<div  id="moncompte" class="sectioncompte">
		<div class="souscompte">
			<!--<h2 class="titreformulaire">Votre compte <?= $_SESSION['client1']->prenom;?> <?= $_SESSION['client1']->nom; ?></h2>	-->
			<?php require('frise1.php');?>
		</div><!--fin souscompte-->

	<div class="filactu">

		<?php
			if($_SESSION['opportunite']->step_1 == 1){
				require('step1.php');
			}else{
				require('step0.php');
			}
		?>

		<?php
		if($_SESSION['opportunite']->step_2 == 1){
			require('step2.php');
		}
		?>

		<?php
		if($_SESSION['opportunite']->step_3 == 1){
			require('step3.php');
		}
		?>

		<?php
		if($_SESSION['opportunite']->step_4 == 1){
			require('step4-1.php');
		}
		?>

	</div> <!--fin filactu-->

	</div> <!--fin sectioncompte-->

</div>





<?php require('../inc/footer.php'); ?>