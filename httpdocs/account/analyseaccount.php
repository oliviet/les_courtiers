<?php require('../inc/header.php'); ?>
<?php require('calculateur.php'); ?>

<div class="pagecompte">

	<?php require('menu_account.php');?>

	<div  id="moncompte" class="sectioncompte">

		<div class="souscompte">

			<?php if(isset($_SESSION['id']) AND $user['id'] == $_SESSION['id']); ?>
		
				<div class="recapinfo">
					<h2>Analyse de votre dossier</h2>

					<div style="display: flex; justify-content: space-between;">

						<?php require('analyse/graph_revenu.php');?>
						
						<?php require('analyse/graph_charge.php');?>
					
						
					</div>

					<?php require('analyse/graph_analyse.php');?>

					
					<br />

					<h2>Analyse de votre projet</h2>

					<div style="display: flex; justify-content: space-between;">

						<?php require('analyse/graph_plan_financement.php');?>
					
						<?php require('analyse/graph_propal_1.php');?>

					</div>


					

				</div><!--fin div recinfo-->
		</div><!--fin div sous compte-->
	</div><!--fin div section compte-->


</div><!--fin div pagecompte-->


<?php require('../inc/footer.php'); ?>