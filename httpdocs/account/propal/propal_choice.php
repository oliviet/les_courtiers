<?php
require_once '../../inc/functions.php';
require_once '../../inc/db.php';
session_start();


$propal_choice=$_POST['propal_choice'];
$propal_1=$_SESSION['opportunite']->id_propal_1;
$propal_2=$_SESSION['opportunite']->id_propal_2;
$propal_3=$_SESSION['opportunite']->id_propal_3;
$id=$_SESSION['opportunite']->id_opportunite;



if($propal_choice == 'propal_1'){

	$req = $pdo->prepare('UPDATE opportunite SET id_propal_choice= ?, step_2= ? WHERE id_opportunite= ?');
	$req->execute([$propal_1, 1,$id]);
	$_SESSION['flash']['success'] = 'La proposition n°1 à été sélectionnée';
	header('Location: ../../compte1.php');
	exit();
};
if($propal_choice == 'propal_2'){
	$req = $pdo->prepare('UPDATE opportunite SET id_propal_choice= ?, step_2= ? WHERE id_opportunite= ?');
	$req->execute([$propal_2, 1, $id]);
	$_SESSION['flash']['success'] = 'La proposition n°2 à été sélectionnée';
	header('Location: ../../compte1.php');
	exit();
};
if($propal_choice == 'propal_3'){
	$req = $pdo->prepare('UPDATE opportunite SET id_propal_choice= ?, step_2= ? WHERE id_opportunite= ?');
	$req->execute([$propal_3, 1, $id]);
	$_SESSION['flash']['success'] = 'La proposition n°3 à été sélectionnée';
	header('Location: ../../compte1.php');
	exit();
};