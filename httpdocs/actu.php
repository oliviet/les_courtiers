<?php include("inc/header.php"); ?>

<div class="div2">
		
			<h1 class="titre_div">Actualités</h1>
		
		</div>
		
		<div class="pagenews">
		
			<div class="tribune">
				
				<section class="article">
				
					<h2 class="titrearticle">Est-il interdit de stationner devant son propre garage ?</h2>
					
					<p class="datearticle">01/01/2018</p>
					
					<img src="image/interdiction_stationner.jpg" class="photoarticle" alt="Photo Gateau"/>
					
					<p class="textarticle">Est-il interdit de stationner devant son propre garage ?
					Oui, selon la Cour de cassation. En effet, cette dernière a 
					rendu dans un arrêt du 20 juin 2017 cette décision et ceci en ces mots :
					"Est considéré comme gênant pour la circulation...
					</p>
					
					<p class="boutonarticle">En lire plus</p>
				
				</section>
				
				<section class="article">
				
					<h2 class="titrearticle">Titre de cette section: h2</h2>
					
					<p class="datearticle">01/01/2018</p>
					
					<img src="image/gateau.jpg" class="photoarticle" alt="Photo Gateau"/>
					
					<p class="textarticle">En ce jour Investio SAS est à six mois d'activité.
					Six mois qui furent bien remplies. La société a pu acquérir 
					plusieurs biens immobiliers ayant un fort potentiel d'investissement.
					Elle a dû faire face à de nombreux défis qu'elle a toujours su relever..
					</p>
					
					<p class="boutonarticle">En lire plus</p>
				
				</section>
				
				<section class="article">
				
					<h2 class="titrearticle">Nouvelle acquisition</h2>
					
					<p class="datearticle">01/01/2018</p>
					
					<img src="image/signature1.jpg" class="photoarticle" alt="Photo"/>
					
					<p class="textarticle">La société a acquis 7 emplacements de parkings à 
					Nemours en Seine-et-Marne dans une résidence de quinze appartements.
					C'est un investissement idéal selon nos critères d'investissements 
					avec une rentabilité locative autour de 20% et avec une...
					</p>
				
					<p class="boutonarticle">En lire plus</p>
				
				</section>
			
			</div>
			
			<div class="colone">
				
				<div class="divcolonne">
				
					<section class="sectioncolonne">
					
						<h2 class="colonnetitre">A l'affiche</h2>
					
						<img src="image/signature1.jpg" class="phototribune" alt="Photo"/>
					
						<h2 class="titrearticlecolonne">Titre de cette article</h2>
					
						<p class="datecolonne">01/01/2018</p>
					
					</section>
				
				</div>
					
				<div class="divcolonne">
				
					<section class="sectioncolonne">
					
						<h2 class="colonnetitre">Récents</h2>
							
							<div class="articlecolonne">
							
								<img src="image/interdiction_stationner.jpg" class="phototribune" alt="Photo"/>
					
								<h2 class="titrearticlecolonne">Titre de cette article 1</h2>
					
								<p class="datecolonne">01/01/2018</p>
							
							</div>
												
							<div class="sectioncolonne">
							
								<img src="image/gateau.jpg" class="phototribune" alt="Photo Gateau"/>
					
								<h2 class="titrearticlecolonne">Titre de cette article 2</h2>
					
								<p class="datecolonne">01/01/2018</p>
							
							</div>
																	
							<div class="sectioncolonne">
							
								<img src="image/signature1.jpg" class="phototribune" alt="Photo"/>
					
								<h2 class="titrearticlecolonne">Titre de cette article 3</h2>
					
								<p class="datecolonne">01/01/2018</p>
							
							</div>
							
							
					
					</section>
				
				</div>
				
				<div class="divcolonne">
				
					<section class="sectioncolonne">
					
						<h2 class="colonnetitre">Nous suivre</h2>
						
						<img src="image/logofacebooknoirrond.jpg" class="logoreseauxsociauxactualites" alt="Logo Facebook"/>
						
						<img src="image/logotwitternoirrond.png" class="logoreseauxsociauxactualites" alt="Logo Twitter"/>
						
						<img src="image/logolinkedin.jpg" class="logoreseauxsociauxactualites" alt="Logo Linked"/>
						
						
					
					</section>
				
				</div>
			</div>
		
		</div>

<br />

<?php include("inc/footer.php"); ?>