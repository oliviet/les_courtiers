<?php

if(isset($_POST['ci']) == NULL)
{
    exit("Montant du prêt non complété");
}

if(isset($_POST['n']) == NULL)
{
    exit("Durée non complété");
}

if(isset($_POST['t']) == NULL)
{
    exit("Taux du prêt non rentré");
}

if(isset($_POST['ta']) == NULL)
{
    exit("Taux d'assurance non rentré");
}


// On déclare une variable

$resultat = (($_POST['ci'] * $_POST['t'] / 1200) /(1- pow(1+$_POST['t']/1200, -$_POST['n']*12))) + ($_POST['ta']/1200*$_POST['ci']);
			/* ($_POST['ci'] * $_POST['t'] / 1200) / (pow (1-(1+$_POST['t']/1200),-($_POST['n']*12)))*/

$massurance = $_POST['ta']/1200*$_POST['ci'];

$coutassurance = $massurance*$_POST['n']*12;

$couttotal = $resultat*$_POST['n']*12-$_POST['ci'];


$resultat= number_format($resultat, 2, ',', ' ');
$massurance= number_format($massurance, 2, ',', ' ');
$coutassurance= number_format($coutassurance, 2, ',', ' ');
$couttotal= number_format($couttotal, 2, ',', ' ');


header("Location: http://localhost/membres/simulateurmensualite.php?resultat=$resultat&massurance=$massurance&coutassurance=$coutassurance&couttotal=$couttotal");
?>