<?php include("inc/header.php"); ?>

<br /><br /><br />

<div id="article-index"><h3>SOMMAIRE</h3><ul>
		<li class="toclink active">
			<a href="/a-propos-de-Les Courtiers.com/mentions-legales.html" class="toclink active">Mentions légales</a>
		</li>
		<li><a href="/a-propos-de-Les Courtiers.com/mentions-legales/nos-comparateurs-information.html" class="toclink">Nos comparateurs - Information</a></li><li><a href="/a-propos-de-Les Courtiers.com/mentions-legales/offre-assurance-auto-1-mois-d-assurance-offert.html" class="toclink">Offre assurance auto - 1 mois d'assurance offert</a></li></ul></div>


<p>Les Courtiers.com, société par actions simplifiée au capital social de XXXX €
<br />Siège social : 54 rue de Jules Ferry 92700 Colombes
<br />Tél. : 01 XX XX XX XX - Fax : XX XX XX XX XX
<br />RCS Nanterre n° xxx xxx xxx
<br />Sous le contrôle de l'ACPR, 61 rue Taitbout 75436 Paris Cedex 09 (www.acpr.banque-france.fr). Les Courtiers.com SAS a souscrit une assurance responsabilité civile professionnelle auprès d’Allianz, 1 Cours Michelet - CS 30051 - 92076 Paris La Défense Cedex. Cette assurance couvre les sinistres survenus dans le monde entier.</p>
<p>Les Courtiers.com SAS est immatriculé au registre de l'ORIAS sous le n°XX XXX XXX (www.orias.fr) en tant que Courtier en assurance, Courtier en opérations de banque et services de paiement (crédits immobiliers et regroupements de crédits), Mandataire d'intermédiaire en opérations de banque et en services de paiement (financement professionnel) et Mandataire non exclusif en opérations de banque et en services de paiement (crédits à la consommation). Les Courtiers.com est soumis au Code monétaire et financier en tant qu'intermédiaire en opérations de banque et services de paiement et au Code des assurances en tant qu'intermédiaire en assurance. Les Courtiers.com SAS déclare avoir enregistré au cours de l'année 2015 une part supérieure au tiers de son chiffre d'affaires au titre de l'activité en assurance avec l'établissement Axa.</p>
<p>Directeur de la publication : Thibaut DELAUNAY, Président<br />L'hébergement du site est assuré par Les Courtiers.com SAS.</p>
<h2>Droits d'auteur - Copyright</h2>
<p>Tous les éléments, notamment les textes, images, photographies, illustrations, sons, musiques, mis en ligne sur le site Les Courtiers.com sont, sauf mentions particulières, la propriété de la société Les Courtiers.com SAS. La marque Les Courtiers.com et son logo sont déposés par la société Les Courtiers.com SAS.</p>
<p>En conséquence et en application des dispositions du Code de la propriété intellectuelle, des dispositions législatives et réglementaires de tous pays et des conventions internationales, toute représentation et / ou reproduction, intégrale ou partielle, de l'un quelconque des éléments mentionnés ci-dessus, faite sans le consentement préalable et écrit de la société Les Courtiers.com SAS, est interdite.</p>
<h2>Informatique et libertés</h2>
<p>Le site de Les Courtiers.com (www.Les Courtiers.com ) est déclaré auprès de la Commission Nationale Informatique et libertés (CNIL-France) en application de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés.</p>
<p>Conformément à l'article 27 de la loi Informatique et libertés du 6 janvier 1978, Les Courtiers.com SAS vous informe que les données personnelles recueillies sur son site font l'objet de traitements automatisés.</p>
<p>Il s'agit notamment des données personnelles collectées lors de :</p>
<ul>
<li>1. la connexion au site Les Courtiers.com par cookies,</li>
<li>2. l'envoi d'un message e-mail à l'un de nos services,</li>
<li>3. l'envoi d'un formulaire en ligne de demande d'informations concernant l'un de nos produits ou services,</li>
<li>4. l'abonnement à notre newsletter,</li>
<li>5. les demandes relatives à votre compte client.</li>
</ul>
<p>Chaque formulaire présent sur le site comporte à la fois des champs obligatoires, nécessaires au traitement de votre demande (coordonnées, employeur, banque actuelle, revenus, montant du crédit...), et des champs facultatifs nous permettant de mieux vous connaître et ainsi mieux répondre à votre demande. Le défaut de réponse dans l'un de ces champs obligatoires entraîne l'impossibilité pour nos services de traiter votre demande.</p>
<p>En effet, nos <a href="/a-propos-de-Les Courtiers.com/nos-partenaires-bancaires.html">banques partenaires</a> ont besoin de tous ces renseignements pour pouvoir vous proposer le meilleur crédit au meilleur taux. Sans ces éléments, votre dossier serait incomplet et donc rejeté par nos partenaires bancaires. La collecte de toutes ces informations vous concernant a pour but de mieux répondre à vos attentes et de vous fournir les services les plus adaptés à votre profil.</p>
<p>Le cookie (petit texte envoyé par le site à votre navigateur et stocké sur votre ordinateur), ne nous permet pas de vous identifier, mais il enregistre des informations relatives à la navigation de votre ordinateur sur notre site. Il permet également d'établir une session pendant la durée de connexion sur notre site, utile lors de la procédure de demande de prêt en ligne. Vous pouvez toutefois paramétrer votre logiciel de navigation afin d'être informé préalablement de l'envoi d'un cookie par notre site et être ainsi en mesure de l'accepter ou de le refuser. Toutefois, nous vous informons que votre refus d'accepter un cookie est susceptible d'empêcher l'accès à certaines parties du site Les Courtiers.com.</p>
<p>Les Courtiers.com est destinataire de ces données personnelles mais peut être amenée à les transmettre à des tiers tels que des partenaires commerciaux. Vous pouvez vous opposer à ce transfert en utilisant le formulaire mis à votre disposition. Les Courtiers.com pourra vous faire parvenir de façon régulière des messages électroniques d'informations générales sur le crédit ou l'assurance ou des messages électroniques personnalisés, en fonction des informations saisies sur notre site. Quel que soit le mode de collecte des données personnelles utilisé, vous disposez d'un droit d'accès, de modification et d'opposition pour des motifs légitimes au traitement des informations qui vous concernent, et ce conformément à la loi Informatique et libertés du 6 janvier 1978.&nbsp;Pour l'exercer, un formulaire est à votre disposition <a href="http://www.Les Courtiers.com/a-propos-de-Les Courtiers.com/contactez-nous.html?orgSelect=5&amp;terrSelect=sub_5_1">sur le site</a>. <br />Si vous ne souhaitez plus recevoir d'emails de notre part, un lien de désabonnement est présent dans tous nos emails.</p>
<p>Pour répondre à ses obligations légales et réglementaires, Les Courtiers.com met en œuvre des traitements de surveillance ayant pour finalité la lutte contre le blanchiment de capitaux et le financement du terrorisme ainsi que l'application des sanctions financières. Ces traitements sont conformes à l'autorisation unique AU003 publiée par la Commission nationale de l'informatique et des libertés auprès de laquelle ils ont été déclarés.</p>
<p>Par ailleurs, les utilisateurs du site sont informés, en application de l'article Article L.223-2 du code de la consommation tel que modifié par la loi n° 2014-344 du 17 mars 2014 dite loi Hamon sur la consommation en son article 9, de la possibilité d'inscrire leur numéro de téléphone sur une liste d'opposition au démarchage téléphonique en dehors des cas de relations contractuelles pré-existantes. Ce service est disponible à l'adresse http://www.bloctel.gouv.fr</p>
<h2>Cookies / Vie privée</h2>
<h3>1. Qu’est-ce qu’un cookie ?</h3>
<p>Un cookie (ou témoin de connexion) est un ensemble de données stocké sur le disque dur de votre terminal (ordinateur, tablette, mobile) par le biais de votre logiciel de navigation à l'occasion de la consultation d'un service en ligne. <br /><br /> Les cookies enregistrés lorsque vous visitez notre site ne vous reconnaissent pas personnellement en tant qu'individu, mais reconnaissent seulement l'appareil que vous êtes en train d'utiliser. Les cookies ne stockent aucune donnée personnelle sensible, mais donnent simplement une information sur votre navigation de façon à ce que votre terminal puisse être reconnu plus tard. <br /><br /> Les cookies ne causent aucun dommage à votre appareil, mais permettent plus facilement par exemple de retrouver vos préférences, de pré-remplir certains champs et d'adapter le contenu de nos services. <br /><br /> Vous seul(e) choisissez si vous souhaitez avoir des cookies enregistrés sur votre appareil et vous pouvez facilement contrôler l'enregistrement des cookies. Pour une information spécifique sur la gestion et le contrôle des cookies, veuillez-vous référer à la rubrique</p>
<h3>2. Quels sont les cookies présents sur ce site ?</h3>
<h4>a. Les cookies statistiques (ou analytiques)</h4>
<p>Les cookies analytiques que nous émettons nous permettent de mieux connaître nos sites et d'améliorer nos services en établissant des statistiques et volumes de fréquentation et d'utilisation (rubriques et contenus visités, parcours...).</p>
<h4>b. Les cookies fonctionnels</h4>
<p>Les cookies fonctionnels que nous émettons nous permettent :</p>
<ul>
<li>d'adapter la présentation de nos sites aux préférences d'affichage de votre terminal (langue utilisée, résolution d'affichage, système d'exploitation utilisé, etc), selon les matériels et les logiciels de visualisation ou de lecture que votre terminal comporte ;</li>
<li>d'améliorer votre expérience utilisateur en pré-remplissant certains champs de nos formulaires</li>
</ul>
<h3>3. Vos choix concernant les cookies :</h3>
<p>Vous pouvez à tout moment choisir de désactiver ces cookies. Votre navigateur peut également être paramétré pour vous signaler les cookies qui sont déposés dans votre ordinateur et vous demander de les accepter ou pas. <br /><br /> Vous pouvez accepter ou refuser les cookies au cas par cas ou bien les refuser systématiquement. <br /><br /> Nous vous rappelons que le paramétrage est susceptible de modifier vos conditions d'accès à nos contenus et services nécessitant l'utilisation de cookies. <br /><br /> Si votre navigateur est configuré de manière à refuser l'ensemble des cookies, vous ne pourrez pas profiter d'une partie de nos services. Afin de gérer les cookies au plus près de vos attentes nous vous invitons à paramétrer votre navigateur en tenant compte de la finalité des cookies. <br /><br /> La gestion des cookies, pour les autoriser, les supprimer ou les configurer par site internet, se fait au sein de votre navigateur, dans des menus spécifiques - vous trouverez toutes les informations dans la section Aide de votre navigateur.</p>
<h2>Enregistrement des conversations téléphoniques</h2>
<p>Les conversations téléphoniques avec nos conseillers téléphoniques Les Courtiers.com sont enregistrées, à des fins de formation, en vue de l'amélioration de la qualité de nos services.<br />Ces informations sont destinées à Les Courtiers.com, responsable du traitement et sont nécessaires à la gestion de votre demande. Vous pouvez vous opposer à cet enregistrement en le signalant au conseiller téléphonique.<br />Quel que soit le mode de collecte des données personnelles utilisé, vous disposez d'un droit d'accès, de modification et d'opposition pour des motifs légitimes au traitement des informations qui vous concernent, et ce conformément à la loi Informatique et libertés du 6 janvier 1978.&nbsp;Pour l'exercer, un formulaire est à votre disposition <a href="http://www.Les Courtiers.com/a-propos-de-Les Courtiers.com/contactez-nous.html?orgSelect=5&amp;terrSelect=sub_5_1">sur le site</a>.</p>
<h2>Vos droits</h2>
<p>Conformément à la législation, "aucun versement, de quelque nature que ce soit, ne peut être exigé d'un particulier, avant l'obtention d'un ou plusieurs prêts d'argent", article L. 321-2 du Code de la consommation.</p>
<p>Jusqu'à l'acceptation de l'offre par l'emprunteur, aucun versement, sous quelque forme que ce soit, ne peut, au titre de l'opération en cause, être fait par le prêteur à l'emprunteur ou pour le compte de celui-ci, ni par l'emprunteur au prêteur. Jusqu'à cette acceptation, l'emprunteur ne peut, au même titre, faire aucun dépôt, souscrire ou avaliser aucun effet de commerce, ni signer aucun chèque. Si une autorisation de prélèvement sur compte bancaire ou postal est signée par l'emprunteur, sa validité et sa prise d'effet sont subordonnées à celle du contrat de crédit.</p>
<p>Vous êtes ainsi protégé et vous conservez la liberté de changer d'avis quand vous le souhaitez. Vous choisissez le prêt qui vous convient le mieux pour votre projet immobilier.</p>
<p>Conformément à l'article R519-30 du Code monétaire et financier, Les Courtiers.com vous informe qu'elle perçoit du partenaire bancaire, au titre de l'intermédiation pour le crédit immobilier, une rémunération correspondant à 1,20% maximum de la somme financée et pouvant être soumise à un plafond.</p>
<h2>Réclamations</h2>
<h3>1. Qu'est-ce qu'une réclamation ?</h3>
<p>Une réclamation est une déclaration par laquelle vous manifestez votre mécontentement envers notre société, sur un ou des sujets clairement identifiés, dans l’application de la convention d’intermédiation.</p>
<h3>2. Comment faire une réclamation ?</h3>
<p>La réclamation prend obligatoirement la <strong>forme d’un écrit</strong> :</p>
<ul>
<li>adressé <strong>par voie postale</strong> à l’adresse :<br />Les Courtiers.com<br />Service de traitement des réclamations<br />36, rue de Saint-Pétersbourg<br />75008 Paris</li>
<li>ou en utilisant le <a href="http://www.Les Courtiers.com/a-propos-de-Les Courtiers.com/contactez-nous.html?orgSelect=6">formulaire de contact</a> mis à votre disposition <strong>sur notre site</strong></li>
</ul>
<h3>3. Comment est traitée votre réclamation ?</h3>
<p>Nous nous engageons à accuser réception de votre réclamation dans un délai maximum de 10 jours ouvrables à compter de sa réception.<br />Nous nous engageons à vous apporter une réponse (positive ou négative) dans un délai de deux mois à compter de la réception de la totalité des éléments de votre réclamation.<br /><br />Si vous n’êtes pas satisfait de notre réponse et si vous êtes un particulier, vous pouvez saisir le médiateur de l’APIC via :</p>
<ul>
<li>le site www.mediateur-apic.com</li>
<li>ou à l’adresse suivante :<br />Monsieur Laurent Denis – Médiateur APIC<br />4 Quater rue de l’Ermitage<br />78 000 VERSAILLES</li>
</ul>
<p>Vous pouvez consulter la Charte de la médiation sur le site internet www.mediateur-apic.com, rubrique «&nbsp;Contact et liens&nbsp;».</p>

	


</div>


<?php include("inc/footer.php"); ?>