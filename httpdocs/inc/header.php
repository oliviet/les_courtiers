<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}

	require('db_junction.php');
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="https://lescourtiers.com/css/style1.css" />
		<title>Les Courtiers.com: Crédit immobilier, Aquisition, Locatif, Rachat, Renégociation </title>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119903501-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

 			gtag('config', 'UA-119903501-1');
		</script>

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" language="javascript" charset="utf-8" src="https://www.lescourtiers.com/javascript/nav.js"></script>
		<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhxhPLuVxzKfzyyno-MgqCGy5mkfQmwvA&callback=initMap" type="text/javascript"></script>-->
		<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyAhxhPLuVxzKfzyyno-MgqCGy5mkfQmwvA" type="text/javascript"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>

 		<script src="https://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
		<script type="text/javascript">

			var value = 1234567.5556; 

				function Arrondir( nomber, nbApVirg ) { 
					return ( parseInt(nomber * Math.pow(10,nbApVirg) + 0.5) ) / Math.pow(10,nbApVirg); 
				} 

			function formatMillier( nombre ) { 
				var nbrArrnd = Arrondir(nombre, 2); 
				return new Intl.NumberFormat().format( nbrArrnd ); 
				} 

				console.log( formatMillier( value ) ); 
		</script>
	</head>
	
	<body id="body">


<header>
		<div id="toptop">
			<div>
				<a href ="https://www.lescourtiers.com/index.php">
				<img src="https://lescourtiers.com/image/logo_lescourtiers.PNG" id="logo" alt="Logo les Courtiers.com"/>
				</a>
			</div>

			<div class="menubandcon">
				<div id='divbandeau'>
					<ul>
						<li><a href='https://www.lescourtiers.com/a_propos.php'>A propos</a></li>
						<li><a href='https://www.lescourtiers.com/espace_presse.php'>Espace Presse</a></li>
						<li><a href='https://www.lescourtiers.com/contact.php'>Contact</a></li>

					</ul>
				</div> <!--fin divbandeau -->


				<div id="divconnexion">
					<ul>
					<?php if (isset($_SESSION['auth'])): ?>
						<li><a href="https://www.lescourtiers.com/account/compte.php">Mon compte</a></li>
						<li><a href="https://www.lescourtiers.com/connexion/logout.php">Se déconnecter</a></li>
					<?php else: ?>
						<li><a href="https://www.lescourtiers.com/connexion/login.php">Se connecter</a></li>
					<?php endif; ?>
					</ul>
						
					<?php if (isset($_SESSION['auth'])): ?>
						<h2 href="https://www.lescourtiers.com/account/compte.php" id="profilheader">Bienvenue<br /><?= $_SESSION['client1']->prenom; ?> <?= $_SESSION['client1']->nom; ?></h2>
					<?php else: ?>
					
				<?php endif; ?>

				</div> <!--fin divconnexion -->
			</div> <!--fin menubandcon -->
		</div> <!--fin toptop -->
		
		<style>
		#fondmenu{
		width: 100% ;
		height: 50px;
		background: linear-gradient( #181b1d , #0c0e0f );
		box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.2),
					0px 1px 0px 1px rgba(255, 255, 255, 0.5) inset,
					0px -3px 0px 1px rgba(0, 0, 0, 0.1) inset;
		}
		.fixedTop{
			position: fixed;
			top:0;
			width: 100%;
			margin-left: auto;
			margin-right: auto;
			z-index: 100;
		}

		</style>

		<div id="fondmenu">	
			<ul id="menu">
				<li><a href="https://www.lescourtiers.com/creditimmo.php"> Crédit Immo</a></li>
				<li><a href="https://www.lescourtiers.com/ade.php"> Assurances</a></li>
				<li><a href="https://www.lescourtiers.com/rac.php"> Rachat de crédit</a></li>
				<li><a href="https://www.lescourtiers.com/creditpro.php"> Crédit Pro</a></li>
				<li><a href="https://www.lescourtiers.com/actu.php">Actualités</a></li>
				<li><a href="https://www.lescourtiers.com/guides.php"> Guides</a></li>
		
			</ul>
		</div>

		<script>
		var positionElementInPage = $('#fondmenu').offset().top;
		$( window ).resize(function() {
		    positionElementInPage = $('#fondmenu').offset().top;
		});
		$(window).scroll(
			function() {
				if ($(window).scrollTop() > positionElementInPage) {
				 // fixed
				$('#fondmenu').addClass("fixedTop");
				} else {
					// unfixed
					$('#fondmenu').removeClass("fixedTop");
				}
			});
		</script>


		<?php if(isset($_SESSION['flash'])): ?>
		<?php foreach($_SESSION['flash'] as $type => $message): ?>
			<div class="alert alert-<?= $type;?>">
				<?= $message; ?>
			</div>
		<?php endforeach; ?>
		<?php unset($_SESSION['flash']); ?>
	<?php endif; ?>
</header>



