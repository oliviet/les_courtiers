<div class="tuile_graph" style="width: 47.5%;">
	<h3>Revenus mensuel</h3>

	<div id="graph_revenu" ></div>

	<div id="donut-revenu" style="height: 300px; width: 90%;margin-top: 40px; margin-bottom: 40px;"></div>

	<script type="text/javascript">
		/*
		* Play with this code and it'll update in the panel opposite.
		*
		* Why not try some of the options above?
		*/

		var salaire_1 = <?php echo json_encode($salaire_1); ?>;
		var prime_1 = <?php echo json_encode($prime_1_arr); ?>;
		var salaire_2 = <?php echo json_encode($salaire_2); ?>;
		var prime_2 = <?php echo json_encode($prime_2); ?>;
		var loyer_futur = <?php echo json_encode($loyer_futur); ?>;
		var loyer_futur = <?php echo json_encode($loyer_futur); ?>;

		Morris.Donut({
			element: 'donut-revenu',
			data: [
				{label: "Salaire 1", value: salaire_1},
				{label: "Prime 1", value: prime_1},
				/*{label: "Salaire 2", value: salaire_2},
				{label: "Prime 2", value: salaire_2},*/
				{label: "Locatif", value: loyer_futur},
				{label: "Locatif futur", value: loyer_futur}
			],
			formatter: function (x) { return x + " €"},
		});

	</script>

	<div class="tuile_graph_table">

		<div>
			<h3>Emprunteur</h3>
			<p>n°1</p>
			<?php
				$nb_emprunteur = $_SESSION['opportunite']->nb_emprunteur;
				if( $nb_emprunteur == 2){
					echo "<p>n°2</p>";
				}?>
		</div>

		<div>
			<h3>Salaire</h3>
			<p><?= $salaire_1_arr ?> €</p>
			<?php
				if( $nb_emprunteur == 2){
					echo $salaire_2 . '€';
				}
			?>
		</div>

		<div>
			<h3>Primes</h3>
			<p><?=$prime_1_arr?> €</p>
			<?php
				if( $nb_emprunteur == 2){
					echo $prime_2 . '€';
				}
			?>
		</div>

		<div>
			<?php
				if( $loyer_futur != 0){
					echo "<h3>Locatif</h3>";
					echo $loyer_futur . '€';
				}
			?>
		</div>

		<div>
			<?php
				if( $loyer_futur != 0){
					echo "<h3>Locatif futur</h3>";
					echo $loyer_futur . '€';
				}
			?>
		</div>

		<div>
			<h3>Total</h3>
			<p><?= $total_revenu_arr ?> €</p>
		</div>	
			
	</div>
</div>