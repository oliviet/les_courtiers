<div class="account_menu">

			<ul>

				<li><a Class="account_menu_id" href="compte.php"><p><?= $_SESSION['client1']->prenom;?> <?= $_SESSION['client1']->nom;?></p></a>
				<li><p Class="account_menu_titre">Votre dossier</p>
					<ul>
						<li><a Class="account_menu_sous-titre" href="accountfile.php">Votre Projet</a></li>
						<li><a Class="account_menu_sous-titre" href="analyseaccount.php">Analyse</a></li>
						<li><a Class="account_menu_sous-titre" href="accountmyaccount.php">Mon compte</a></li>
						<li><a Class="account_menu_sous-titre" href="account_doc.php">Mon Dossier</a></li>
					</ul>
				</li>
				
				<li><p Class="account_menu_titre">Simulateur</p>
					<ul>
						<li><a Class="account_menu_sous-titre" href="simulateurmensualite.php">Mensualité</a></li>
						<li><a Class="account_menu_sous-titre" href="simulateurcapaciteemprunt.php">Capacité d'endettement</a></li>
						<li><a Class="account_menu_sous-titre" href="">Capacité d'emprunt</a></li>
						<li><a Class="account_menu_sous-titre" href="">Rachat de crédit</a></li>
					</ul>
				</li>
				<li><p Class="account_menu_titre">Contact</p>
					<ul>
						<li><a Class="account_menu_sous-titre" href="accountcourtier.php">Votre Courtier</a></li>
						<li><a Class="account_menu_sous-titre" href="">Support</a></li>
						<li><a Class="account_menu_sous-titre" href="">Une réclamation?</a></li>
						<li><a Class="account_menu_sous-titre" href="">Donnez-nous votre avis</a></li>
					</ul>
				</li>
				<li><p Class="account_menu_titre">Parrainage</p>
					<ul>
						<li><a Class="account_menu_sous-titre" href="">Devenir parrain</a></li>
						<li><a Class="account_menu_sous-titre" href="">Vos parrainages</a></li>
					</ul>
				</li>
			</ul>
		</nav>
</div><!--fin menu compte-->