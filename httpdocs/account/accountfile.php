<?php
session_start();

require ('../inc/functions.php');

logged_only();


?>

<?php require('../inc/header.php'); ?>


<div class="pagecompte">
<?php require('menu_account.php');?>
	<div  id="moncompte" class="sectioncompte">
		<div class="souscompte">

			<?php if(isset($_SESSION['id']) AND $user['id'] == $_SESSION['id']); ?>
		
				<div class="recapinfo">

					<h2 id="votredossier">Votre Dossier</h2>

					<p>Ici consultez vos informations déclaratives du formulaire.</p>
					<p>Complétez votre dossier en nous transmettant les documents demandés.</p>

					<h2>Votre Projet</h2>

						<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['rac']->id_rac)) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Montant prêt initial : </h3>'.$_SESSION['rac']->montant_pret_initial.' €';
									echo '<h3 class="titreformulaire">Taux prêt initial :</h3>'.$_SESSION['rac']->taux_pret_initial.' % ';
									echo '<h3 class="titreformulaire">Durée initiale :</h3>'.$_SESSION['rac']->dureeinitiale.' mois';
									echo '<h3 class="titreformulaire">Capital restant dû :</h3>'.$_SESSION['rac']->capital_restant_du.' €';
									echo '<h3 class="titreformulaire">Durée restante :</h3>'.$_SESSION['rac']->duree_restante.' mois </div>';
								}
								else {
									echo '<div class="sousinfo"><h3 class="titreformulaire">Type de projet :</h3>'.$_SESSION['opportunite']->type_projet;
									echo '<h3 class="titreformulaire">Type de bien :</h3>'.$_SESSION['opportunite']->type_bien;
									echo '<h3 class="titreformulaire">Type de d\'investissement :</h3>'.$_SESSION['opportunite']->type_invest;
									echo '<h3 class="titreformulaire">Etat d\'avancement :</h3>'.$_SESSION['opportunite']->avancement.'</div>';
								}
							?>

						<?php /* on teste si notre variable est égale à NULL*/
								if (empty($_SESSION['opportunite']->montant_acquisition == 0 )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Montant acquisition : </h3>'.$_SESSION['opportunite']->montant_acquisition.' €</div>';
									
								};
							?>

							<?php /* on teste si notre variable est égale à NULL*/
								if (empty($_SESSION['opportunite']->montant_construction == 0 )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Montant construction : </h3>'.$_SESSION['opportunite']->montant_construction.' €</div>';
									
								};
							?>

							<?php /* on teste si notre variable est égale à NULL*/
								if (empty($_SESSION['opportunite']->frais_notaire == 0 )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Frais de notaire : </h3>'.$_SESSION['opportunite']->frais_notaire.' €</div>';
									
								};
							?>

							<?php /* on teste si notre variable est égale à NULL*/
								if (empty($_SESSION['opportunite']->montant_travaux == 0 )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Montant travaux : </h3>'.$_SESSION['opportunite']->montant_travaux.' €</div>';
									
								};
							?>

							<?php /* on teste si notre variable est égale à NULL*/
								if (empty($_SESSION['opportunite']->futur_loyer == 0 )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Futur loyer : </h3>'.$_SESSION['opportunite']->futur_loyer.' €</div>';
									
								};
							?>
						
					<h2 id="situationperso">Situation Personnelle</h2>
					<div class="deuxsousinfo">
						<div class="petitesousinfo">
						<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client2'])) {
								/*on affiche le résultat*/
									echo '<h3>Emprunteur n°1</h3>';
									echo '<div class="sousinfo"><h3 class="titreformulaire">Civilite : </h3>'.$_SESSION['client1']->civilite;
									echo '<h3 class="titreformulaire">Nom : </h3>'.$_SESSION['client1']->nom;
									echo '<h3 class="titreformulaire">Prénom : </h3>'.$_SESSION['client1']->prenom;
									echo '<h3 class="titreformulaire">Nationalité : </h3>'.$_SESSION['client1']->nationalite;
									echo '<h3 class="titreformulaire">Date de naissance : </h3>'.$_SESSION['client1']->naissance;
									echo '<h3 class="titreformulaire">Téléphone : </h3>'.'0'.$_SESSION['client1']->telephone.'</div>';	
								};
							?>
						</div>
						
						<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client2']->civilite)) {
								/*on affiche le résultat*/
									echo '<div class="petitesousinfo">';
									echo '<h3>Emprunteur n°2</h3>';
									echo '<div class="sousinfo"><h3 class="titreformulaire">Civilite : </h3>'.$_SESSION['client2']->civilite;
									echo '<h3 class="titreformulaire">Nom : </h3>'.$_SESSION['client2']->nom;
									echo '<h3 class="titreformulaire">Prénom : </h3>'.$_SESSION['client2']->prenom;
									echo '<h3 class="titreformulaire">Nationalité : </h3>'.$_SESSION['client2']->nationalite;
									echo '<h3 class="titreformulaire">Date de naissance : </h3>'.$_SESSION['client2']->naissance;
									echo '<h3 class="titreformulaire">Téléphone : </h3>'.'0'.$_SESSION['client2']->telephone.'</div>';
									echo '</div>';

								};
							?>
						
					</div>
					<h2 id="situationpro">Situation Professionelle</h2>

					<div class="deuxsousinfo">
						<div class="petitesousinfo">
						<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client1'] )) {
								/*on affiche le résultat*/
									echo '<h3>Emprunteur n°1</h3>';
									echo '<div class="sousinfo"><h3 class="titreformulaire">Salaire net mensuel : </h3>'.$_SESSION['client1']->salaire_net_mois.' € ';
									echo '<h3 class="titreformulaire">Prime annuelle : </h3>'.$_SESSION['client1']->prime_net_an.' € ';
									echo '<h3 class="titreformulaire">Profession : </h3>'.$_SESSION['client1']->situation_pro;
									echo '<h3 class="titreformulaire">Contrat de travail : </h3>'.$_SESSION['client1']->type_contrat;
									echo '<h3 class="titreformulaire">Date debut contrat : </h3>'.$_SESSION['client1']->debut_contrat.'</div>';
									

								};
							?>
						</div>
						
							<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client2']->civilite )) {
								/*on affiche le résultat*/
									echo '<div class="petitesousinfo">';
									echo '<h3>Emprunteur n°2</h3>';
									echo '<div class="sousinfo"><h3 class="titreformulaire">Salaire net mensuel : </h3>'.$_SESSION['client2']->salaire_net_mois.' € ';
									echo '<h3 class="titreformulaire">Prime annuelle : </h3>'.$_SESSION['client2']->prime_net_an.' € ';
									echo '<h3 class="titreformulaire">Profession : </h3>'.$_SESSION['client2']->situation_pro;
									echo '<h3 class="titreformulaire">Contrat de travail : </h3>'.$_SESSION['client2']->type_contrat;
									echo '<h3 class="titreformulaire">Date debut contrat : </h3>'.$_SESSION['client2']->debut_contrat.'</div>';
									echo '</div>';

								};
							?>
						
					</div>

					<h2 id="situtationpatri">Situation patrimoniale</h2>

						<div class="sousinfo">
							<h3 class="titreformulaire">Logement actuel : </h3><?= $_SESSION['client1']->logement_actuel; ?>
						</div>
						

					
							
							<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client1']->montant_loyer )) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire"> Loyer actuel : </h3>'.$_SESSION['client1']->montant_loyer.' € </div>';
								};
							?>
							
						
						


						<div class="sousinfo">
							<h3 class="titreformulaire">Nombre de bien(s) immobilier(s) : </h3><?= $_SESSION['client1']->nb_bien_immo; ?>
						</div>

						<div class="deuxsousinfo">
							<div class="petitesousinfo">

								<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['bien_immo_1']->mensualite)) {
										/*on affiche le résultat*/
											echo '<h3>Bien Immobilier n°1</h3>';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Estimation : </h3>'.$_SESSION['bien_immo_1']->valeur.' €';
											echo '<h3 class="titreformulaire">Mensualité : </h3>'.$_SESSION['bien_immo_1']->mensualite.' €';
											echo '<h3 class="titreformulaire">Durée : </h3>'.$_SESSION['bien_immo_1']->duree_restante.' mois</div>';
											

										};
									?>
							</div>
							<div class="petitesousinfo">
									<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['bien_immo_2']->mensualite)) {
										/*on affiche le résultat*/
											echo '<h3>Bien Immobilier n°2</h3>';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Estimation : </h3>'.$_SESSION['bien_immo_2']->valeur.' €';
											echo '<h3 class="titreformulaire">Mensualité : </h3>'.$_SESSION['bien_immo_2']->mensualite.' €';
											echo '<h3 class="titreformulaire">Durée : </h3>'.$_SESSION['bien_immo_2']->duree_restante.' mois</div>';
											

										};
									?>
							</div>
						</div>
						<div class="deuxsousinfo">
							<div class="petitesousinfo">
								<?php /* on teste si notre variable est égale à NULL*/
									if (isset($_SESSION['bien_immo_3']->mensualite)) {
									/*on affiche le résultat*/
										echo '<h3>Bien Immobilier n°3</h3>';
										echo '<div class="sousinfo"><h3 class="titreformulaire">Estimation : </h3>'.$_SESSION['bien_immo_3']->valeur.' €';
										echo '<h3 class="titreformulaire">Mensualité : </h3>'.$_SESSION['bien_immo_3']->mensualite.' €';
										echo '<h3 class="titreformulaire">Durée : </h3>'.$_SESSION['bien_immo_3']->duree_restante.' mois</div>';
										

									}
									else;
								?>
							</div>
							<div class="petitesousinfo">
								<?php /* on teste si notre variable est égale à NULL*/
									if (isset($_SESSION['bien_immo_4']->mensualite)) {
									/*on affiche le résultat*/
										echo '<h3>Autre(s) Bien(s) Immobilier(s)</h3>';
										echo '<div class="sousinfo"><h3 class="titreformulaire">Estimation autre(s) bien(s): </h3>'.$_SESSION['bien_immo_4']->valeur.' €';
										echo '<h3 class="titreformulaire">Mensualité autre(s) crédit(s): </h3>'.$_SESSION['bien_immo_4']->mensualite.' €';
										echo '<h3 class="titreformulaire">Durée autre(s) crédit(s): </h3>'.$_SESSION['bien_immo_4']->duree_restante.' mois</div>';
										

									};
								?>
							</div>
						</div>

					
						<div class="deuxsousinfo">
							<div class="petitesousinfo">
								<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['credit_conso_1']->mensualite)) {
										/*on affiche le résultat*/
											echo '<h3>Crédit consommation n°1 :</h3>';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Mensualité : </h3>'.$_SESSION['credit_conso_1']->mensualite.' €';
											echo '<h3 class="titreformulaire">Durée : </h3>'.$_SESSION['credit_conso_1']->duree_restante.' mois</div>';

										};
									?>
							</div>
							<div class="petitesousinfo">
								<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['credit_conso_2']->mensualit)) {
										/*on affiche le résultat*/
											echo '<h3>Crédit consommation n°2 :</h3>';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Mensualité : </h3>'.$_SESSION['credit_conso_2']->mensualite.' €';
											echo '<h3 class="titreformulaire">Durée : </h3>'.$_SESSION['credit_conso_2']->duree_restante.' mois</div>';

										};
									?>
							</div>
							<div class="petitesousinfo">
								<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['credit_conso_3']->mensualite)) {
										/*on affiche le résultat*/

											echo '<h3>Autre Crédit Consommation :</h3>';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Mensualité: </h3>'.$_SESSION['credit_conso_3']->mensualite.' €';
											echo '<h3 class="titreformulaire">Durée(s): </h3>'.$_SESSION['credit_conso_3']->duree_restante.' mois</div>';

										};
									?>
							</div>
						</div>


							
								
									<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['client1']->vension_recue)) {
										/*on affiche le résultat*/
											echo '<div class="petitesousinfo">';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Pension reçue : </h3>'.$_SESSION['client1']->vension_recue.' € </div>';
											echo '</div>';
										};
									?>
							
							
								
									<?php /* on teste si notre variable est égale à NULL*/
										if (isset($_SESSION['client1']->vension_verse)) {
										/*on affiche le résultat*/
											echo '<div class="petitesousinfo">';
											echo '<div class="sousinfo"><h3 class="titreformulaire">Pension versée : </h3>'.$_SESSION['client1']->pension_verse.' € </div>';
											echo '</div>';
										};
								
									?>
								
							

						<?php /* on teste si notre variable est égale à NULL*/
								if (isset($_SESSION['client1']->alloc_fami)) {
								/*on affiche le résultat*/
									echo '<div class="sousinfo"><h3 class="titreformulaire">Allocations familiales : </h3>'.$_SESSION['client1']->alloc_fami.' € </div>';
								}
							?>



					<h2 id="precisionprojet">Précision sur le projet</h2>

					<div class="sousinfo">
						<h3 class="titreformulaire">Commentaire : </h3><?= $_SESSION['opportunite']->commentaire; ?>
					</div>

					<div class="sousinfo">
						<h3 class="titreformulaire">Parrain : </h3><?= $_SESSION['opportunite']->code_parrain; ?>
					</div>
		
				</div><!--fin div recinfo-->
		</div><!--fin div sous compte-->
	</div><!--fin div section compte-->






</div><!--fin div pagecompte-->


<?php require('../inc/footer.php'); ?>