<?php require("inc/header.php"); ?>

<div id="affiche">
	

	<div id="affiche2">

		<p id="affiche3">A l'affiche</p>

		<div class="carousel">
			<div class="carousel-under1"">
				<img class="affiche_img" src="https://lescourtiers.com/image/affiche.png" alt="affiche"/>

			</div>
			<div class="carousel-under2">
				
					<h2>5 minutes et 12 secondes<br/>TOP CHRONO !!!</h3>
					<div>
						<a class="champignon" href="https://www.lescourtiers.com/formulaire/formulaireimmo.php"><p>Déposer votre dossier</p></a>
					</div>
					
			</div>
		</div>			
		<div id="affiche4">	
		</div>
	</div>	
</div> <!--Fin affiche -->
		
		<div id="divbarometre">	

			<p id="barometrebegin"> Les <strong>meilleurs taux</strong></p>

			<ul id="barometre">
				<li>7 ans <br /><em>1,0 %</em></li>
				<li>10 ans<br /><em>1,4 %</em></li>
				<li>15 ans<br /><em>1,8 %</em></li>
				<li>20 ans<br /><em>2,0 %</em></li>
				<li>25 ans<br /><em>2,5 %</em></li>
			</ul>

			<a id="barometreend" href=https://www.lescourtiers.com/index.php><strong>La Bourse des Taux</strong></a>

		</div>

		<div class="lign">

			<h2>Les Simulateurs</h2>

			<div class="colum">

				<div class="block_md_25">
					<div class="simu_button">
						<a href="simulateur/capacite_emprunt.php">
							<img class="simu_img" src="image/bar-chart.png">
							<p>Capacité d'emprunt</p>
						</a>
					</div>
				</div>

				<div class="block_md_25">
					<div class="simu_button">
					<a href="simulateur/mensualite.php">
						<img class="simu_img" src="image/agenda.png">
						<p>Mensualité</p>
					</a>
					</div>
				</div>
				
				<div class="block_md_25">
					<div class="simu_button">
						<a href="simulateur/mensualite.php">
							<img class="simu_img" src="image/percent-symbol.png">
							<p>Taux d'endettement</p>
						</a>
					</div>
				</div>
				
				<div class="block_md_25">
					<div class="simu_button">
						<a href="simulateur/mensualite.php">
							<img class="simu_img" src="image/accounting.png">
							<p>Rachat de Crédit</p>
						</a>
					</div>
				</div>
			</div>
		</div>		
		
		<div class="lign">
			
			<h2>Le contrat : Les Courtiers.com</h2>
				
				<div class="colum">
					<div class="block_md_33">
						<div class="pave">	
							<div class="pave_2_yellow">					
								<img src="image/tirelire-icone.png" alt="Icone tirelire"/>
								<div class="divpt">
									<h3>Gratuit</h3>
									<p><span class="gras">Aucun frais, aucun honoraires,</span> aucune petite étoile</p>
								</div>										
							</div>
							<div class="pave_2_yellow">								
								<img src="image/colombe-icone.png" alt="Icone Colombe"/>
									<div class="divpt">
										<h3>Sans engagement</h3>
										<p>A tout instant vous êtes <span class="gras">libre d'arrêter</span>.</p>
									</div>
							</div>
						</div>
					</div>
					<div class="block_md_33">
						<div class="pave">
							<div class="pave_2_yellow">		
								<img src="image/telephone-icone.png" alt="Icone Téléphone"/>
								<div class="divpt">
									<h3>Disponibilité</h3>
									<p>Par téléphone, par mail, par courrier, par signaux de fumée, nous répondrons</p>
								</div>
							</div>
							<div class="pave_2_yellow">
								<img src="image/alarm-clock.png" alt="Icone Horloge"/>
								<div class="divpt">
									<h3>Réactivité</h3>
									<p>Notre capacité de <span class="gras">réagir et d'adpatation</span> sont sans limites</p>
								</div>
							</div>
						</div>
					</div>
					<div class="block_md_33">
						<div class="pave">
							<div class="pave_2_yellow">
								<img src="image/wrench-icone.png" alt="Icone Clé"/>
								<div class="divpt">
									<h3>Optimisation</h3>
									<p>Votre Courtier va vous construire <span class="gras">un crédit sur mesure</span></p>
								</div>	
							</div>	
							<div class="pave_2_yellow">									
								<img src="image/dossier.png" alt="Icone Dossier"/>
								<div class="divpt">
									<h3>Montage du dossier</h3>
									<p>Un accompagnement appronfondi pour un <span class="gras">dossier en béton</span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div><!-- lign 1 -->
		
		

		<div class="lign">
			<h2 id="observatoire">L'<span class="jaune">O</span>bservatoire par les <span class="jaune">Courtiers</span>.<span class="jaune">com</span></h2>
			<div class="colum">
				<div class="block_md_33">
					<div class="menuobservatoire">
						<h3 class="titreobservatoire">Actualités :</h3>

						<div class="lignactu">
							<p>20/01:</p><a class="lienactu" href=###>Crédit : Une augmentation de...</a>
						</div>
						<div class="lignactu">
							<p>20/01:</p><a class="lienactu" href=###>Crédit : Une augmentation de...</a>
						</div>
						<div class="lignactu">
							<p>20/01:</p><a class="lienactu" href=###>Crédit : Une augmentation de...</a>
						</div>
						<div class="lignactu">
							<p>20/01:</p><a class="lienactu" href=###>Crédit : Une augmentation de...</a>
						</div>	
					</div>
				</div><!-- block_md_30-->

				<div class="block_md_33">
					<div class="menuobservatoire">
						<h3 class="titreobservatoire">Bourse des Taux</h3>
						<div id="area-example" style="height: 180px;"></div>
						<script type="text/javascript">
								
								/*
								 * Play with this code and it'll update in the panel opposite.
								 *
								 * Why not try some of the options above?
								 */
								Morris.Area({
								element: 'area-example',
								data: [
									/*{ y: '2012-12', c:3.13, b:3.40, a:3.70},*/
									{ y: '2013-12', c:3.03, b:3.33, a:3.65},
									{ y: '2014-12', c:2.21, b:2.50, a:2.83},
									{ y: '2015-12', c:2.03, b:2.31, a:2.65},
									{ y: '2016-12', c:1.18, b:1.40, a:1.65},
									{ y: '2017-02', c:1.30, b:1.52, a:1.79},
									{ y: '2017-12', c:1.32, b:1.52, a:1.79},
									{ y: '2018-02', c:1.31, b:1.48, a:1.76}
								],
								xkey: 'y',
								ykeys: ['a', 'b', 'c'],
								labels: ['15ans', '20ans', '25ans'],
								hideHover: 'auto',
								ymax : '4',
								postUnits: ['%'],
								fillOpacity: [0.00],
								behaveLikeLine: 'false',
								});


						</script>

						<div class="lignactu">
							<a class="lienactu" href=###>Venez découvrir l'évolution des taux !</a>
						</div>
					</div>
				</div><!-- block_md_30-->

				<div class="block_md_33">
					<div class="menuobservatoire">
						<h3 class="titreobservatoire">Guides à l'affiche</h3>
						<div class="lignactu">
							<a class="lienactu" href=###>&bull; PTZ : Prêt à Taux Zéro</a>
						</div>
						<div class="lignactu">
							<a class="lienactu" href=###>&bull; PAS : Prêt Accession Sociale</a>
						</div>
						<div class="lignactu">
							<a class="lienactu" href=###>&bull; Les Frais de Notaire</a>
						</div>
						<div class="lignactu">
							<a class="lienactu" href=###>&bull; Courtier en crédit</a>
						</div>
					</div>
				</div><!-- block_md_30-->					
			</div><!-- colum-->	
		</div> <!-- lign 3 -->

		<div id="divcredit">
	
			<h4 id="credit">Un crédit vous engage et doit être remboursé. Vérifiez vos capacités de remboursement avant de vous engager.</p>

		</div>

<?php require("inc/footer.php"); ?>