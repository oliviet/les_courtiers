<footer>
	<!--	-->
		<div id="divfooter">
		
		<div id="contact">
			<ul>
				<li><a href="https://lescourtiers.com/mentionslegales.php">Mentions légales</a></li>
				<li><a href="https://lescourtiers.com/contact.php">Contact</a></li>
				<li><a href="https://lescourtiers.com/espacepresse.php">Espace presse</a></li>
				<li><a href="https://lescourtiers.com/recrutement.php">Recrutement</a></li>
				<li><a href="https://lescourtiers.com//formulaire/formulaireimmo.php">Déposer un dossier</a></li>
				<li><a href="https://lescourtiers.com/account/compte.php">Votre compte</a></li>
			</ul>
		</div>

		<div id="reseaux">
								
						<h2 class="colonnetitre">Nous suivre</h2>
						
						<a href="https://www.facebook.com/"><img src="https://lescourtiers.com/image/logofacebooknoirrond.jpg" class="logoreseauxsociauxactualites" alt="Logo Facebook"/></a>
						
						<a href="https://www.twitter.com/"><img src="https://lescourtiers.com/image/logotwitternoirrond.png" class="logoreseauxsociauxactualites" alt="Logo Twitter"/></a>
						
						<a href="https://fr.linkedin.com/"><img src="https://lescourtiers.com/image/logolinkedin.jpg" class="logoreseauxsociauxactualites" alt="Logo Linked"/></a>
		</div>
		
			
		
	</div>
		
	<p id="copyright">Copyright 2018</p>

</footer>

</body>


</html>