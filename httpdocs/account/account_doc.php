<?php
session_start();

require ('inc/functions.php');

logged_only();


?>

<?php require('inc/header.php'); ?>

<div class="pagecompte">
	<?php require('account/menu_account.php');?>
	
	<div style="margin-left: 1%;width: 80%; ">
		<br/>
		<h2>Votre Dossier</h2>

		<div class="table_account">
			<div class="lign_account">

				<div class="case_account_1">
						<p>Compromis de vente</p>
					</div>
					<div class="case_account_2">
						<p>Elément principale de votre dossier. Nos partenaires bancaires commence à travailler sur un dossier de crédit seulement si un compromis est signé.</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_compromis_fn)){
									echo $_SESSION['up']->up_compromis_fn;
									if($_SESSION['up']->up_compromis_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_compromis.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
			</div><!--fin lign_account-->
			<br/>
			<h2>Situation personnelle</h2>

			<div class="lign_account">

				<div class="case_account_1">
						<p>Pièce d'identité</p>
					</div>
					<div class="case_account_2">
						<p>Carte d'identité nationnal ou le passport.</p>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
			</div><!--fin lign_account-->
			<div class="lign_account">

				<div class="case_account_1">
						<p>Justificatif de domicile</p>
					</div>
					<div class="case_account_2">
						<p>Facture electricite, gaz, quittance de loyer de moins de 3 mois</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_compromis_fn)){
									echo $_SESSION['up']->up_compromis_fn;
									if($_SESSION['up']->up_compromis_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_compromis.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
			</div><!--fin lign_account-->
			<div class="lign_account">
				<div class="case_account_1">
						<p>Livret de famille</p>
					</div>
					<div class="case_account_2">
						<p>Le livret de familles avec la page enfant.</p>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
			</div><!--fin lign_account-->
			<br/>
			<h2>Situation professionnelle</h2>
			<div class="lign_account">
				<div class="case_account_1">
						<p>Contrat de travail</p>
					</div>
					<div class="case_account_2">
						<p>Le contrat de travail permet d'analyse la situtation pro (période d'essai, prime, 13ème mois...)</p>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
						

					</div>
			</div><!--fin lign_account-->
			<div class="lign_account">
				<div class="case_account_1">
						<p>Fiche de salaire</p>
					</div>
					<div class="case_account_2">
						<p>Les trois derniers bulletins de salaire, si vous avez des primes annuelles rajouter le bulletins de salaire de fin d'année</p>
					</div>
					<div class="case_account_4">
						
					</div>
					<div class="case_account_4">
						
					</div>


			</div><!--fin lign_account-->
			<div class="lign_account">
				<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
							
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
							
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
							
					</div>
					<div class="case_account_4">
						<?php if(isset($_SESSION['up']->up_pi_2_fn)){
									echo $_SESSION['up']->up_pi_2_fn;
									if($_SESSION['up']->up_pi_2_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_2.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
							
					</div>


			</div><!--fin lign_account-->
			<br/>
			<h2>Situation patrimonial</h2>
			<div class="lign_account">
				<div class="case_account_1">
						<p>Avis d'imposition</p>
					</div>
					<div class="case_account_2">
						<p>L'avis d'imposition permettra d'analyser l'ensemble de vos revenus</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
					


			</div><!--fin lign_account-->
			<div class="lign_account">
				<div class="case_account_1">
						<p>Taxe Foncière</p>
					</div>
					<div class="case_account_2">
						<p>La taxe foncière permettra d'analyser votre patrimoine immobiliers</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
				<div class="lign_account">
					<div class="case_account_1">
						<p>Baux de location</p>
					</div>
					<div class="case_account_2">
						<p>L'ensemble de vos baux de location pour déterminer vos revenus locatifs</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
				<div class="lign_account">
					<div class="case_account_1">
						<p>Synthèse des comptes</p>
					</div>
					<div class="case_account_2">
						<p>La synthèse de vos comptes pour toutes les banques</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
				<div class="lign_account">
					<div class="case_account_1">
						<p>Relevé de compte</p>
					</div>
					<div class="case_account_2">
						<p>Les trois derniers relevé de tout vos comptes courants</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									echo $_SESSION['up']->up_pi_1_fn;
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
				<div class="lign_account">
					<div class="case_account_1">
						<p>Pension versé</p>
					</div>
					<div class="case_account_2">
						<p>Justificatifs des pensions versées</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									
									echo $_SESSION['up']->up_pi_1_fn;
									
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
				<div class="lign_account">
					<div class="case_account_1">
						<p>Pension reçus</p>
					</div>
					<div class="case_account_2">
						<p>Justificatifs des pensions reçues</p>
					</div>
					<div class="case_account_3">
						<?php if(isset($_SESSION['up']->up_pi_1_fn)){
									
									echo $_SESSION['up']->up_pi_1_fn;
									
									if($_SESSION['up']->up_pi_1_ev == 1){
										echo "<br/><p style=\"color: green;\">Validé</p>";
									}else{
										echo "<br/><p style=\"color: orange;\">En cours de validation</p>";
									};
								}else{
									echo "<form method=\"post\" action=\"account/download_file/reception_fichier_pi_1.php\" enctype=\"multipart/form-data\">
										<label for=\"mon_fichier\">Tous formats | max. 1 Mo :</label>
										<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"1048576\" />
										<input type=\"file\" name=\"mon_fichier\" id=\"mon_fichier\" />
										<input type=\"submit\" name=\"submit\" value=\"Envoyer\" />
										</form>";
								}; ?>
					</div>
				</div><!--fin lign_account-->
		</div><!--fin table_account-->
		<br/>
	</div>
</div><!--fin pagecompte-->
<?php require('inc/footer.php'); ?>